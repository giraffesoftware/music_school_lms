<?php


namespace App\Services;


class EKLassService
{
    private $clientId, $clientSecret, $redirectUri, $httpClient;

    public $eClassUrl = 'https://my.e-klase.lv/Auth/OAuth/';

    public function __construct()
    {
        $this->clientId = config('e-klass.client_id');
        $this->clientSecret = config('e-klass.client_secret');
        $this->redirectUri = config('e-klass.redirect_uri');

        $this->httpClient = new \GuzzleHttp\Client(['base_uri' => $this->eClassUrl]);
    }

    const userInfoResponseCodeDescription = [
        10 => 'Internal Error',
        20 => 'not Authorized',
        30 => 'Bad Request',
        40 => 'invalid Application ID',
        50 => 'invalid User ID',
        60 => '=> invalid Session ID',
    ];

    public function getAccessToken(string $code)
    {
        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $this->redirectUri,
            'code' => $code,
        ];

        $request = $this->httpClient->get('GetAccessToken?' . http_build_query($params));
        $response = $request->getBody()->getContents();

        //on success string returned, on error json object
        $errors = json_decode($response);

        if ($errors) {
            throw new \Exception($errors->error->message);
        }

        parse_str($response, $res);

        return $res['access_token'];
    }


    /**
     * returns user object in SimpleXMLElement format e.g.
     * "ID": "ID"
     * "FirstName": "Jānis"
     * "LastName": "Tests"
     * "PersonType": "Student"
     * "SchoolId": "SchoolId"
     * "School": "SchoolDescription"
     * "ClassId": "classId"
     * "ClassLevel": "1"
     * "ClassName": "1. kl. akordeonisti"
     * "PupilClassMembershipDateFrom": "2020-01-29T00:00:00+02:00"
     * @param $accessToken
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function getUserInfo($accessToken): \SimpleXMLElement
    {
        $params = [
            'access_token' => $accessToken,
        ];

        $request = $this->httpClient->get('API/ME?' . http_build_query($params));
        $response = $request->getBody()->getContents();

        $xml = simplexml_load_string($response);

        if ($xml->code) {
            throw new \Exception(self::userInfoResponseCodeDescription[intval($xml->code)]);
        }
        return $xml;
    }
}