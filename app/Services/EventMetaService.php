<?php


namespace App\Services;


use App\Entities\EventMeta\EventMeta;
use App\Repositories\Attendings\AttendingsRepository;
use App\Repositories\EventMeta\EventMetaRepository;
use App\Repositories\RoomReservations\RoomReservationsRepository;
use Carbon\Carbon;

class EventMetaService
{
    private $roomReservationsRepository;
    private $attendingsRepository;
    private $eventMetaRepository;

    public function __construct(RoomReservationsRepository $roomReservationsRepository,
                                AttendingsRepository $attendingsRepository, EventMetaRepository $eventMetaRepository)
    {
        $this->roomReservationsRepository = $roomReservationsRepository;
        $this->attendingsRepository = $attendingsRepository;
        $this->eventMetaRepository = $eventMetaRepository;
    }

    /**
     * if event is ok - create event and return it's info, else - return event data with errors
     *
     * @param EventMeta $eventMeta
     * @param array $eventData
     * @return array
     */
    public function createSingleReservationFromMetaTry(EventMeta $eventMeta, array $eventData): array
    {
        $data = $eventData;
        $data['start_datetime'] = $this->makeDatetimeFromDateAndTime($eventMeta->start_date, $eventMeta->start_time);
        $data['end_datetime'] = $this->makeDatetimeFromDateAndTime($eventMeta->start_date, $eventMeta->end_time);
        $data['event_meta_id'] =$eventMeta->id;

        $userIds = array_merge($data['teachers'], $data['students']);

        $errors = $this->roomReservationsRepository->getReservationDataErrors($userIds, $data['room_id'],
            Carbon::parse($data['start_datetime']), Carbon::parse($data['end_datetime']));

        if (!empty($errors)) {
            $this->eventMetaRepository->deleteEventMeta($eventMeta->id);

            return [
                'errors' => $errors
            ];
        }

        $roomReservation = $this->roomReservationsRepository->storeReservation($data);

        $this->attendingsRepository->storeAttendings([
            "students" => $eventData['students'],
            "teachers" => $eventData['teachers'],
            "reservation_id" => $roomReservation->id
        ]);

        $undraftSuccess = $this->eventMetaRepository->undraftEventMetaTry($eventMeta);

        $result = $this->eventMetaRepository->getEventMeta($eventMeta->id);

        if (!$undraftSuccess) {
            //delete event meta if it is not successfully undrafted
            $this->eventMetaRepository->deleteEventMeta($eventMeta->id);
        }

        return $result;
    }

    /**
     * @param EventMeta $eventMeta
     * @param array $eventData
     * @return array
     */
    public function draftRecursiveReservationsFromMeta(EventMeta $eventMeta, array $eventData): array
    {
        $eventData['event_meta_id'] = $eventMeta->id;
        $eventData['is_single'] = false;

        $currentDate = $eventMeta->start_date;

        $data = $eventData;
        $firstWeekLessonDates = [];
        $firstWeekFinishDate = $currentDate->copy()->addWeek();
        while ($currentDate->lte($eventMeta->end_date) && $currentDate->lt($firstWeekFinishDate)) {
            if (in_array($currentDate->dayOfWeek, $eventMeta->week_days)) {
                $firstWeekLessonDates[] = $currentDate->copy();
            }

            $currentDate->addDay();
        }

        for($i=0;;$i++){
            foreach ($firstWeekLessonDates as $firstWeekLessonDate){
                $currentDate = $firstWeekLessonDate->copy()->addWeeks($eventMeta->interval * $i);

                if($currentDate->gt($eventMeta->end_date)){
                    break 2;
                }

                $data['start_datetime'] = $this->makeDatetimeFromDateAndTime($currentDate, $eventMeta->start_time);
                $data['end_datetime'] = $this->makeDatetimeFromDateAndTime($currentDate, $eventMeta->end_time);

                $roomReservation = $this->roomReservationsRepository->storeReservation($data);

                $this->attendingsRepository->storeAttendings([
                    "students" => $eventData['students'],
                    "teachers" => $eventData['teachers'],
                    "reservation_id" => $roomReservation->id
                ]);
            }
        }

        return $this->eventMetaRepository->getEventMeta($eventMeta->id);
    }

    protected function makeDatetimeFromDateAndTime(Carbon $date, string $time)
    {
        return Carbon::parse($date->toDateString() . ' ' . $time);
    }
}
