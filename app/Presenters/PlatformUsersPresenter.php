<?php

namespace App\Presenters;

use App\Transformers\PlatformUsersTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PlatformUsersPresenterPresenter.
 *
 * @package namespace App\Presenters;
 */
class PlatformUsersPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PlatformUsersTransformer();
    }
}
