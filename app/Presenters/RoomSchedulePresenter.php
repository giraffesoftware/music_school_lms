<?php

namespace App\Presenters;

use App\Transformers\RoomScheduleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomSchedulePresenter.
 *
 * @package namespace App\Presenters;
 */
class RoomSchedulePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RoomScheduleTransformer();
    }
}
