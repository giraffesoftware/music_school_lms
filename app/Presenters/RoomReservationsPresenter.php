<?php

namespace App\Presenters;

use App\Transformers\RoomReservationsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomReservationsPresenter.
 *
 * @package namespace App\Presenters;
 */
class RoomReservationsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RoomReservationsTransformer();
    }
}
