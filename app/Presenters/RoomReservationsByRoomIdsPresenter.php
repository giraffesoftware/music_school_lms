<?php

namespace App\Presenters;

use App\Transformers\RoomReservationsByRoomIdsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomReservationsByRoomIdsPresenter.
 *
 * @package namespace App\Presenters;
 */
class RoomReservationsByRoomIdsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RoomReservationsByRoomIdsTransformer();
    }
}
