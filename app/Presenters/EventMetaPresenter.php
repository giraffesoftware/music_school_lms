<?php

namespace App\Presenters;

use App\Transformers\EventMetaTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomReservationsPresenter.
 *
 * @package namespace App\Presenters;
 */
class EventMetaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        return new EventMetaTransformer();
    }
}
