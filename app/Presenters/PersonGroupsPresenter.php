<?php

namespace App\Presenters;

use App\Transformers\PersonGroupsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PersonGroupsPresenter.
 *
 * @package namespace App\Presenters;
 */
class PersonGroupsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PersonGroupsTransformer();
    }
}
