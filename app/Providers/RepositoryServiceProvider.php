<?php

namespace App\Providers;

use App\Repositories\Attendings\AttendingsRepository;
use App\Repositories\Attendings\AttendingsRepositoryEloquent;
use App\Repositories\PersonGroups\PersonGroupsRepository;
use App\Repositories\PersonGroups\PersonGroupsRepositoryEloquent;
use App\Repositories\PlatformUsers\PlatformUsersRepository;
use App\Repositories\PlatformUsers\PlatformUsersRepositoryEloquent;
use App\Repositories\Status\StatusRepository;
use App\Repositories\Status\StatusRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Categories\CategoriesRepository',
            'App\Repositories\Categories\CategoriesRepositoryEloquent'
        );
        $this->app->bind(
            StatusRepository::class,
            StatusRepositoryEloquent::class
        );
        $this->app->bind(
            'App\Repositories\Subjects\SubjectsRepository',
            'App\Repositories\Subjects\SubjectsRepositoryEloquent'
        );
        $this->app->bind(
            'App\Repositories\RoomReservations\RoomReservationsRepository',
            'App\Repositories\RoomReservations\RoomReservationsRepositoryEloquent'
        );
        $this->app->bind(
            'App\Repositories\RoomGroups\RoomGroupsRepository',
            'App\Repositories\RoomGroups\RoomGroupsRepositoryEloquent'
        );

        $this->app->bind(
            'App\Repositories\EventMeta\EventMetaRepository',
            'App\Repositories\EventMeta\EventMetaRepositoryEloquent'
        );
        $this->app->bind(
            'App\Repositories\Rooms\RoomsRepository',
            'App\Repositories\Rooms\RoomsRepositoryEloquent'
        );
        $this->app->bind(
            PlatformUsersRepository::class,
            PlatformUsersRepositoryEloquent::class
        );
        $this->app->bind(
            PersonGroupsRepository::class,
            PersonGroupsRepositoryEloquent::class
        );
        $this->app->bind(
            AttendingsRepository::class,
            AttendingsRepositoryEloquent::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
