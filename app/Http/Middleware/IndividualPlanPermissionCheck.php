<?php

namespace App\Http\Middleware;

use Closure;

class IndividualPlanPermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');
        $currentUser = auth()->user();

        if ($user->id === $currentUser->id) {
            if (!$currentUser->can('read-self-individual_plan')) {
                return response()->json(['message' => 'access denied'], 403);
            }
        } else {
            if (!$currentUser->can('read-individual_plan')) {
                return response()->json(['message' => 'access denied'], 403);
            }
        }
        return $next($request);
    }
}
