<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Forgot password email send
     *
     * @param  Request  $request
     * @OA\Post(
     *     path="/api/auth/forgot-password",
     *     operationId="auth.forgot-password",
     *     tags={"AuthController"},
     *     description="user forgot password",
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful email sent",
     *          @OA\JsonContent(),
     *       ),
     * )
     */
    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email|exists:platform_users'],
            ['email.*' => 'Wrong email. Please check and retry again']);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'message was already sent, wait up to ' .
            config('auth.passwords.users.expire') . ' minutes'], 400);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json(['message' => 'email was sent']);
    }
}
