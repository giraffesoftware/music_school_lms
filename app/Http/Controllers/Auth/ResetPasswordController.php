<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;
    /**
     * Reset password
     *
     * @param  Request  $request
     * @OA\Post(
     *     path="/api/auth/reset-password",
     *     operationId="auth.reset-password",
     *     tags={"AuthController"},
     *     description="user reset password",
     *     @OA\Parameter(
     *          name="token",
     *          description="email token",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password_confirmation",
     *          description="password confirmation",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful password reset",
     *          @OA\JsonContent(),
     *       ),
     * )
     */

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected function resetPassword($user, $password)
    {
        $user->password = $password;
        $user->save();

        event(new PasswordReset($user));
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email|exists:platform_users',
            'password' => 'required|string|min:6|max:255',
        ];
    }

    protected function sendResetResponse(Request $request, $response)
    {
        return response()->json(['message' => 'password reset successful']);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Link expired'], 422);
    }
}
