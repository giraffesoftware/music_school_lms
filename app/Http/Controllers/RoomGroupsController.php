<?php

namespace App\Http\Controllers;

use App\Entities\Rooms\Rooms;
use App\Repositories\RoomGroups\RoomGroupsRepository;
use Illuminate\Http\Request;

class RoomGroupsController extends Controller
{
    private $roomGroupsRepository;

    public function __construct(RoomGroupsRepository $roomGroupsRepository)
    {
        $this->roomGroupsRepository = $roomGroupsRepository;
    }

    public function getRoomGroupsWithRooms()
    {
        $allGroupRoomsWithRooms = $this->roomGroupsRepository->getAllGroupRoomsWithRooms();

        return response()->json($allGroupRoomsWithRooms->toArray());
    }
}
