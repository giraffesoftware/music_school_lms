<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginUsingEClassRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\PlatformUsers\PlatformUsersRepository;
use App\Role;
use App\Services\EKLassService;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $platformUserRepository, $eKlassService;

    /**
     * Create a new AuthController instance.
     *
     * @param PlatformUsersRepository $platformUserRepository
     * @param EKLassService $eKlassService
     */
    public function __construct(PlatformUsersRepository $platformUserRepository, EKLassService $eKlassService)
    {
        $this->platformUserRepository = $platformUserRepository;
        $this->eKlassService = $eKlassService;
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/auth/login",
     *     operationId="auth.login",
     *     tags={"AuthController"},
     *     description="user login",
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="Password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful login",
     *          @OA\JsonContent(),
     *       ),
     *     @OA\Response(
     *          response=401,
     *          description="wrong data provided",
     *          @OA\JsonContent(),
     *       ),
     * )
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Wrong email or password'], 422);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param LoginUsingEClassRequest $request
     *
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/auth/login-using-e-class",
     *     operationId="auth.login-e-class",
     *     tags={"AuthController"},
     *     description="user login with e-class",
     *     @OA\Parameter(
     *          name="code",
     *          description="code",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful login",
     *          @OA\JsonContent(),
     *       ),
     *     @OA\Response(
     *          response=401,
     *          description="wrong data provided",
     *          @OA\JsonContent(),
     *       ),
     * )
     */
    public function loginUsingEKlass(LoginUsingEClassRequest $request)
    {
        try {
            $token = $this->eKlassService->getAccessToken($request->code);
            $info = $this->eKlassService->getUserInfo($token);
            $userInfo = [
                'name' => $info->FirstName->__toString(),
                'surname' => $info->LastName->__toString(),
                'e_klass_id' => $info->ID->__toString(),
                'e_klass_personal_code' => $info->AdditionalData->DataItem->__toString(),
            ];
            $user = $this->platformUserRepository->skipPresenter()
                ->findWhere(['e_klass_id' => $userInfo['e_klass_id']])->first();

            //if there is no user with this e_klass_id we can login by e_klass_personal_code
            if (empty($user)) {
                $user = $this->platformUserRepository->skipPresenter()
                    ->findWhere(['e_klass_personal_code' => $userInfo['e_klass_personal_code']])->first();

                if ($user) {
                    $user->e_klass_id = $userInfo['e_klass_id'];
                    $user->save();
                }
            }

            if (empty($user)) {
                $user = $this->platformUserRepository->skipPresenter()->create($userInfo);

                $role = Role::where('name', strtolower($info->PersonType))->first();
                $user->attachRole($role);
            }

            $token = $this->guard()->login($user);
            return $this->respondWithToken($token);
        } catch (\Exception $exception) {
            abort(400, $exception->getMessage());
        }
    }

    /**
     * Register user.
     *
     * @param RegisterRequest $request
     *
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/auth/register",
     *     operationId="auth.register",
     *     tags={"AuthController"},
     *     description="user register",
     *     @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="password",
     *          description="Password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="surname",
     *          description="surname",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful login",
     *          @OA\JsonContent(),
     *       ),
     *     @OA\Response(
     *          response=401,
     *          description="wrong data provided",
     *          @OA\JsonContent(),
     *       ),
     * )
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        try {
            $user = $this->platformUserRepository->skipPresenter()->create($data);

            $token = $this->guard()->login($user);
            return $this->respondWithToken($token);

        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Get the authenticated User
     *
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/auth/me",
     *     operationId="auth.me",
     *     tags={"AuthController"},
     *     description="user info",
     *     @OA\Response(
     *          response=200,
     *          description="current user info returned",
     *          @OA\JsonContent(),
     *       ),
     *     @OA\Response(
     *          response=401,
     *          description="unauthorized",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function me()
    {
        return response()->json($this->platformUserRepository->find(auth()->id()));
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
