<?php

namespace App\Http\Controllers;

use App\Repositories\Rooms\RoomsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    private $roomsRepository;

    public function __construct(RoomsRepository $roomsRepository)
    {
        $this->roomsRepository = $roomsRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @OA\Get(
     *     path="/api/rooms/get_schedule",
     *     operationId="roomsSchedule",
     *     tags={"RoomController"},
     *     description="get schedule by rooms array",
     *     @OA\Parameter(
     *          name="rooms[]",
     *          description="Rooms ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_from",
     *          description="Start date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_to",
     *          description="Finish date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getRoomScheduleByPeriod(Request $request)
    {
        $dateFrom = Carbon::parse($request->date_from)->startOfDay();
        $dateTo = Carbon::parse($request->date_to)->endOfDay();
        $rooms = $request->rooms;
        $roomSchedule = $this->roomsRepository->getRoomsSchedule($rooms, $dateFrom, $dateTo);

        return response()->json($roomSchedule, 200);
    }
}
