<?php

namespace App\Http\Controllers;

use App\Repositories\PersonGroups\PersonGroupsRepository;
use App\Repositories\PlatformUsers\PlatformUsersRepository;
use Illuminate\Http\Request;

class PlatformUsersController extends Controller
{
    private $userRepository;

    private $personGroupsRepository;

    public function __construct(PlatformUsersRepository $platformUsersRepository,PersonGroupsRepository $personGroupsRepository)
    {
        $this->userRepository = $platformUsersRepository;
        $this->personGroupsRepository = $personGroupsRepository;

    }

    public function getStudents()
    {
        $students = $this->personGroupsRepository->getGroupsWithStudents();

        return response()->json($students);
    }

    public function getTeachers()
    {
        $teachers = $this->userRepository->getTeachers();

        return response()->json($teachers);
    }
}
