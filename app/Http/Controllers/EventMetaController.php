<?php

namespace App\Http\Controllers;

use App\Entities\EventMeta\EventMeta;
use App\Http\Requests\EventMeta\CreateRequest;
use App\Http\Requests\EventMeta\DraftRequest;
use App\Repositories\EventMeta\EventMetaRepository;
use App\Services\EventMetaService;
use Illuminate\Http\JsonResponse;

class EventMetaController extends Controller
{
    private $eventMetaRepository, $eventMetaService;

    public function __construct(EventMetaRepository $eventMetaRepository, EventMetaService $eventMetaService)
    {
        $this->eventMetaRepository = $eventMetaRepository;
        $this->eventMetaService = $eventMetaService;
    }

    /**
     * Draft events with date ranges and week days
     * @param DraftRequest $request
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/event-meta/draft",
     *     operationId="event-meta.draft",
     *     tags={"EventMetaController"},
     *     description="draft event meta",
     *     @OA\Parameter(
     *          name="students[]",
     *          description="Students ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="teachers[]",
     *          description="Teachers ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="room_id",
     *          description="Room id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="week_days[]",
     *          description="array of lessons week days 0 - 6 (sunday - saturday)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="interval",
     *          description="Interval in weeks (default - 1 week)",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="start_date",
     *          description="Start date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="end_date",
     *          description="End date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="start_time",
     *          description="Start time (e.g. 00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="end_time",
     *          description="End time (e.g. 00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="category_id",
     *          description="Category id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="status_id",
     *          description="Status id (default - booked)",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="subject_id",
     *          description="SUbject id",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function draftEventMeta(DraftRequest $request)
    {
        $eventMeta = $this->eventMetaRepository->storeEventMeta($request->validated(), false);

        $result = $this->eventMetaService->draftRecursiveReservationsFromMeta($eventMeta, $request->validated());

        return response()->json(
            [
                'result' => $result,
                'message' => 'Event meta drafted'
            ]);
    }

    /**
     * undraft event meta reservations if all of them are correct
     * @param EventMeta $eventMeta
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/event-meta/submit/{event_meta}",
     *     operationId="event-meta.submit",
     *     tags={"EventMetaController"},
     *     description="submit event meta",
     *     @OA\Parameter(
     *          name="event_meta",
     *          description="Event meta id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function submitEventMeta(EventMeta $eventMeta)
    {
        if (!$this->eventMetaRepository->undraftEventMetaTry($eventMeta)) {
            //error
            $status = 400;
        }

        return response()->json(
            [
                'result' => $this->eventMetaRepository->getEventMeta($eventMeta->id),
                'message' => 'Event meta submitted'
            ], $status ?? 200);
    }

    /**
     * Create single event
     * @param CreateRequest $request
     * @return JsonResponse
     *
     * * @OA\Post(
     *     path="/api/event-meta/create",
     *     operationId="event-meta.create",
     *     tags={"EventMetaController"},
     *     description="create event meta",
     *     @OA\Parameter(
     *          name="students[]",
     *          description="Students ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="teachers[]",
     *          description="Teachers ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="room_id",
     *          description="Room id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="start_date",
     *          description="Start date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="start_time",
     *          description="Start time (e.g. 00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="end_time",
     *          description="End time (e.g. 00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="category_id",
     *          description="Category id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="status_id",
     *          description="Status id (default - booked)",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="subject_id",
     *          description="Required if category is not maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="event_name",
     *          description="Required if category is maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="description",
     *          description="Can set if category is maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function createEventMeta(CreateRequest $request)
    {
        $data = $request->validated();
        $data['end_date'] = $data['start_date'];
        $eventMeta = $this->eventMetaRepository->storeEventMeta($data);

        $result = $this->eventMetaService->createSingleReservationFromMetaTry($eventMeta, $data);

        if (empty($result['errors'])) {
            return response()->json(
                [
                    'result' => $result,
                    'message' => 'Event meta created'
                ]);
        }

        return response()->json(
            [
                'errors' => $result['errors'],
            ], 400);
    }


    /**
     * delete event meta
     * @param EventMeta $eventMeta
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/event-meta/{event_meta}",
     *     operationId="event-meta.delete",
     *     tags={"EventMetaController"},
     *     description="delete event meta",
     *     @OA\Parameter(
     *          name="event_meta",
     *          description="Event meta id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteEventMeta(EventMeta $eventMeta)
    {
        $this->eventMetaRepository->deleteEventMeta($eventMeta->id);

        return response()->json(
            [
                'message' => 'Event meta deleted'
            ]);
    }


    /**
     * Get event meta by id
     * @param EventMeta $eventMeta
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/event-meta/{event_meta}",
     *     operationId="event-meta.get",
     *     tags={"EventMetaController"},
     *     description="delete event get by id",
     *     @OA\Parameter(
     *          name="event_meta",
     *          description="Event meta id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getEventMeta(EventMeta $eventMeta)
    {
        return response()->json(
            [
                'result' => $this->eventMetaRepository->getEventMeta($eventMeta->id),
                'message' => 'Event meta returned'
            ]);
    }
}
