<?php

namespace App\Http\Controllers;

use App\Entities\PlatformUsers\PlatformUsers;
use App\Entities\RoomReservations\RoomReservations;
use App\Http\Requests\RoomReservation\DeleteMultiplyRequest;
use App\Http\Requests\RoomReservation\GetByUserRequest;
use App\Http\Requests\RoomReservation\UpdateRequest;
use App\Repositories\Attendings\AttendingsRepository;
use App\Repositories\RoomReservations\RoomReservationsRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * @var RoomReservationsRepository
     */
    private $reservationRepository;

    private $attendingsRepository;

    /**
     * ReservationController constructor.
     * @param RoomReservationsRepository $reservationRepository
     * @param AttendingsRepository $attendingsRepository
     */
    public function __construct(RoomReservationsRepository $reservationRepository,
                                AttendingsRepository $attendingsRepository)
    {
        $this->reservationRepository = $reservationRepository;
        $this->attendingsRepository = $attendingsRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createReservation(Request $request)
    {
        $data = $this->reservationRepository->storeReservation($request->all());

        $attendings = $this->attendingsRepository->storeAttendings([
            "students" => $request->students,
            "teachers" => $request->teachers,
            "reservation_id" => $data->id
        ]);

        return response()->json(
            [
                'reservation' => $data,
                'attendings' => $attendings,
                'message' => 'Reservation created'
            ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllReservations()
    {
        $allReservations = $this->reservationRepository->getAll();

        return response()->json($allReservations, 200);
    }

    /**
     * @param RoomReservations $roomReservation
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReservation(RoomReservations $roomReservation)
    {
        $reservation = $this->reservationRepository->getReservation($roomReservation->id);

        return response()->json($reservation["data"][0], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReservationsByDate(Request $request)
    {
        $dateFrom = $request->date_from;
        $dateTo = $request->date_to;
        $reservations = $this->reservationRepository->getReservationByDate($dateFrom, $dateTo);

        return response()->json($reservations, 200);
    }

    /**
     * @param Request $request
     * @param PlatformUsers $user
     * @return \Illuminate\Http\JsonResponse
     * @OA\Get(
     *     path="/api/reservations/by_user/{user}/by_date",
     *     operationId="eventsByUser",
     *     tags={"ReservationController"},
     *     description="get events by user",
     *     @OA\Parameter(
     *          name="user",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_from",
     *          description="Start date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="date_to",
     *          description="Finish date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              format="date"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getReservationsByUserAndDate(GetByUserRequest $request, PlatformUsers $user)
    {
        $dateFrom = $request->date_from;
        $dateTo = $request->date_to;

        $reservations = $this->reservationRepository->getReservationByUserAndDate(
            $user, Carbon::parse($dateFrom), Carbon::parse($dateTo));

        return response()->json($reservations, 200);
    }

    public function getRoomsReservations(Request $request)
    {
        $reservations = $this->reservationRepository->getReservationsByRoomId($request->room_id);

        return response()->json($reservations, 200);
    }

    /**
     * delete single reservation
     * @param RoomReservations $roomReservation
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/reservations/reservation/{roomReservation}",
     *     operationId="room-reservations.delete",
     *     tags={"ReservationController"},
     *     description="delete single reservation",
     *     @OA\Parameter(
     *          name="roomReservation",
     *          description="Room reservation id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteReservation(RoomReservations $roomReservation)
    {
        $this->reservationRepository->deleteReservation($roomReservation->id);

        return response()->json(["message" => 'reservation deleted'], 200);
    }

    /**
     * delete array of reservations
     * @param DeleteMultiplyRequest $request
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/reservations/reservation/multiply",
     *     operationId="room-reservations.delete-multiply",
     *     tags={"ReservationController"},
     *     description="delete array of reservations",
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="Room reservation ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteMultiplyReservations(DeleteMultiplyRequest $request): JsonResponse
    {
        foreach ($request->ids as $id) {
            $this->reservationRepository->deleteReservation($id);
        }

        return response()->json(["message" => 'reservations deleted'], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param RoomReservations $roomReservation
     * @return \Illuminate\Http\JsonResponse
     * @OA\Put(
     *     path="/api/reservations/reservation/{roomReservation}",
     *     operationId="updateReservation",
     *     tags={"ReservationController"},
     *     description="update reservation",
     *     @OA\Parameter(
     *          name="roomReservation",
     *          description="Room reservation id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="students[]",
     *          description="Students ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="teachers[]",
     *          description="Teachers ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="room_id",
     *          description="Room id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="start_datetime",
     *          description="Start datetime (01.01.2019 12:00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="end_datetime",
     *          description="End datetime (01.01.2019 12:00:00)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="category_id",
     *          description="Category id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="status_id",
     *          description="Status id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="subject_id",
     *          description="Required if category is not maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="event_name",
     *          description="Required if category is maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="description",
     *          description="Can set if category is maintenance or concert",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function editReservation(UpdateRequest $request, RoomReservations $roomReservation): \Illuminate\Http\JsonResponse
    {
        $updateData = $request->validated();

        if (!$roomReservation->is_draft) {
            $userIds = array_merge($updateData['teachers'], $updateData['students']);

            $errors = $this->reservationRepository->getReservationDataErrors($userIds, $request['room_id'],
                Carbon::parse($request['start_datetime']), Carbon::parse($request['end_datetime']), [$roomReservation->id]);
        }

        if (empty($errors)) {
            $data = $this->reservationRepository->editReservation($roomReservation, $updateData);

            return response()->json(['reservation' => $data, 'message' => 'Reservation updated']);
        }

        return response()->json(['errors' => $errors], 400);
    }

}
