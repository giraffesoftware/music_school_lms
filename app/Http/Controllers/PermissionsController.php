<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\JsonResponse;
use App\Transformers\RoleTransformer;

class PermissionsController extends Controller
{
    /**
     * get permissions
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/permissions",
     *     operationId="permissions.get",
     *     tags={"PermissionsController"},
     *     description="get all permissions",
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function get()
    {
        return response()->json(config('laratrust_seeder')['role_structure'][auth()->user()->roles()->first()->name]);
    }

    /**
     * get roles
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/roles",
     *     operationId="roles.get",
     *     tags={"PermissionsController"},
     *     description="get all roles",
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getRoles()
    {
        $roles = Role::all()->map(function ($item) {
            return (new RoleTransformer)->transform($item);
        })->toArray();

        return response()->json(['data' => $roles]);
    }
}
