<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\JsonResponse;
use App\Entities\Subjects\Subjects;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subjects\CreateRequest;
use App\Http\Requests\Subjects\UpdateRequest;
use App\Repositories\Subjects\SubjectsRepository;
use App\Http\Requests\Subjects\DeleteMultiplyRequest;

class AdminSubjectController extends Controller
{
    private $subjectRepository;

    const PER_PAGE = 15;

    public function __construct(SubjectsRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * get all subjects
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/subjects",
     *     operationId="getSubjects",
     *     tags={"AdminSubjectController"},
     *     description="get all subjects",
     *     @OA\Parameter(
     *          name="page",
     *          description="Page number",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="search",
     *          description="Search expression",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getSubjects(): JsonResponse
    {
        $allSubjects = $this->subjectRepository->paginate(self::PER_PAGE);
        return response()->json($allSubjects, 200);
    }

    /**
     * get one subject
     * @param Subjects $subject
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/subjects/{subject}",
     *     operationId="getSubject",
     *     tags={"AdminSubjectController"},
     *     description="get one subject",
     *     @OA\Parameter(
     *          name="subject",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getSubject(Subjects $subject): JsonResponse
    {
        $subject = $this->subjectRepository->find($subject->id);

        return response()->json($subject, 200);
    }

    /**
     * create subject
     * @param CreateRequest $request
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/admin/subjects/subject/create",
     *     operationId="createSubject",
     *     tags={"AdminSubjectController"},
     *     description="create subject method",
     *     @OA\Parameter(
     *          name="name",
     *          description="Name subject",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function createSubject(CreateRequest $request): JsonResponse
    {
        $createData = $request->validated();

        $data = $this->subjectRepository->createSubject($createData);

        return response()->json(['subject' => $data, 'message' => 'Subject created'], 200);
    }

    /**
     * update subject
     * @param UpdateRequest $request
     * @param Subjects $subject
     * @return JsonResponse
     * @OA\Put(
     *     path="/api/admin/subjects/subject/{subject}",
     *     operationId="updateSubject",
     *     tags={"AdminSubjectController"},
     *     description="update subject",
     *     @OA\Parameter(
     *          name="subject",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          description="Subject name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function editSubject(UpdateRequest $request, Subjects $subject): JsonResponse
    {
        $updateData = $request->validated();

        $data = $this->subjectRepository->editSubject($subject, $updateData);

        return response()->json(['subject' => $data, 'message' => 'Subject updated'], 200);
    }

    /**
     * delete single subject
     * @param Subjects $subject
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/subjects/subject/{subject}",
     *     operationId="subjects.delete",
     *     tags={"AdminSubjectController"},
     *     description="delete single subject",
     *     @OA\Parameter(
     *          name="subject",
     *          description="Subject id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteSubject(Subjects $subject): JsonResponse
    {
        $this->subjectRepository->deleteSubject($subject->id);
        return response()->json(['message' => 'subject deleted'], 200);
    }

    /**
     * delete array of subjects
     * @param DeleteMultiplyRequest $request
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/subjects/subject/multiply",
     *     operationId="subjects.delete-multiply",
     *     tags={"AdminSubjectController"},
     *     description="delete array of subjects",
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="Subject ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteMultiplySubjects(DeleteMultiplyRequest $request): JsonResponse
    {
        foreach (request('ids') as $subjectId) {
            $this->subjectRepository->deleteSubject($subjectId);
        }
        return response()->json(['message' => 'subjects deleted'], 200);
    }
}
