<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Criteria\OnlyTrashedCriteria;
use App\Http\Controllers\Controller;
use App\Criteria\WithTrashedCriteria;
use App\Entities\PlatformUsers\PlatformUsers;
use App\Http\Requests\PlatformUsers\UpdateRequest;
use App\Http\Requests\PlatformUsers\CreateRequest;
use App\Http\Requests\PlatformUsers\DeleteMultiplyRequest;
use App\Repositories\PlatformUsers\PlatformUsersRepository;

class AdminUserController extends Controller
{
    private $userRepository;

    const PER_PAGE = 15;

    public function __construct(PlatformUsersRepository $platformUsersRepository)
    {
        $this->userRepository = $platformUsersRepository;
    }

    /**
     * get all users
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/users",
     *     operationId="getUsers",
     *     tags={"AdminUserController"},
     *     description="get all users",
     *     @OA\Parameter(
     *          name="page",
     *          description="Page number",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="search",
     *          description="Search expression",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="role_id",
     *          description="Role id for filter",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getUsers(): JsonResponse
    {
        $allUsers = $this->userRepository->getAll(request('search'), request('role_id'))->paginate(self::PER_PAGE);
        return response()->json($allUsers, 200);
    }

    /**
     * get one user
     * @param PlatformUsers $user
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/users/{user}",
     *     operationId="getUser",
     *     tags={"AdminUserController"},
     *     description="get one user",
     *     @OA\Parameter(
     *          name="user",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getUser(PlatformUsers $user): JsonResponse
    {
        $user = $this->userRepository->pushCriteria(WithTrashedCriteria::class)->find($user->id);

        return response()->json($user, 200);
    }

    /**
     * get all archive users
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/users/archive",
     *     operationId="getArchiveUsers",
     *     tags={"AdminUserController"},
     *     description="get all archive users",
     *     @OA\Parameter(
     *          name="page",
     *          description="Page number",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="search",
     *          description="Search expression",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="role_id",
     *          description="Role id for filter",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getArchiveUsers(): JsonResponse
    {
        $allUsers = $this->userRepository
            ->pushCriteria(OnlyTrashedCriteria::class)->getAll(request('search'), request('role_id'))->paginate(self::PER_PAGE);
        return response()->json($allUsers, 200);
    }

    /**
     * update user
     * @param UpdateRequest $request
     * @param PlatformUsers $user
     * @return JsonResponse
     * @OA\Patch(
     *     path="/api/admin/users/user/{user}",
     *     operationId="updateUser",
     *     tags={"AdminUserController"},
     *     description="update user",
     *     @OA\Parameter(
     *          name="user",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          description="User name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="surname",
     *          description="User surname",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="middle_name",
     *          description="User middlename",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="role_ids[]",
     *          description="Roles ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="e_klass_personal_code",
     *          description="User personal code (xxxxxx-xxxxx where x are integers)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="phone_number",
     *          description="User phone number (“+” (optional) and numbers)",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="status",
     *          description="User status (Active/Archived - boolean (true, false))",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function editUser(UpdateRequest $request, PlatformUsers $user): JsonResponse
    {
        $updateData = $request->validated();

        $errors = $this->userRepository->getUpdateUserErrors($updateData['role_ids']);

        if (empty($errors)) {
            $data = $this->userRepository->editUser($user, $updateData);
            return response()->json(['user' => $data, 'message' => 'User updated'], 200);
        }

        return response()->json(['errors' => $errors], 400);
    }

    /**
     * delete single user
     * @param PlatformUsers $user
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/users/user/{user}",
     *     operationId="users.delete",
     *     tags={"AdminUserController"},
     *     description="delete single user",
     *     @OA\Parameter(
     *          name="user",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="force",
     *          description="Flag confirmation for delete",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteUser(PlatformUsers $user): JsonResponse
    {
        $errors = $this->userRepository->getDeleteUserErrors($user, request('force'), Auth::user()->id);

        if (empty($errors)) {
            $this->userRepository->deleteUser($user->id);
            return response()->json(['message' => 'user deleted'], 200);
        }

        return response()->json(['errors' => $errors], 400);
    }

    /**
     * delete array of users
     * @param DeleteMultiplyRequest $request
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/users/user/multiply",
     *     operationId="users.delete-multiply",
     *     tags={"AdminUserController"},
     *     description="delete array of users",
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="User ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="force",
     *          description="Flag confirmation for delete",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteMultiplyUsers(DeleteMultiplyRequest $request): JsonResponse
    {
        $errors = $this->userRepository->getMultiplyDeleteUserErrors(request('ids'), request('force'), Auth::user()->id);

        if (empty($errors)) {
            foreach (request('ids') as $userId) {
                $this->userRepository->deleteUser($userId);
            }
            return response()->json(['message' => 'users deleted'], 200);
        }

        return response()->json(['errors' => $errors], 400);
    }

    /**
     * create user
     * @param CreateRequest $request
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/admin/users/user/create",
     *     operationId="createUser",
     *     tags={"AdminUserController"},
     *     description="create user method",
     *     @OA\Parameter(
     *          name="email",
     *          description="User Email (x@x.x)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          description="User Name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="surname",
     *          description="User Surname",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="middle_name",
     *          description="User Middlename",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="e_klass_personal_code",
     *          description="Personal code (xxxxxx-xxxxx where x are integers)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="role_ids[]",
     *          description="Roles ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="phone_number",
     *          description="User phone number (“+” (optional) and numbers)",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="status",
     *          description="User status (Active/Archived - boolean (true, false))",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function createUser(CreateRequest $request): JsonResponse
    {
        $createData = $request->validated();

        $errors = $this->userRepository->getUpdateUserErrors($createData['role_ids']);

        if (empty($errors)) {
            $data = $this->userRepository->createUser($createData);
            return response()->json(['user' => $data, 'message' => 'User created'], 200);
        }

        return response()->json(['errors' => $errors], 400);
    }
}
