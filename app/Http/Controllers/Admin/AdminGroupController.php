<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Entities\PersonGroups\PersonGroups;
use App\Entities\PlatformUsers\PlatformUsers;
use App\Http\Requests\PersonGroups\CreateRequest;
use App\Http\Requests\PersonGroups\UpdateRequest;
use App\Http\Requests\PersonGroups\AssignRequest;
use App\Http\Requests\PersonGroups\DetachRequest;
use App\Repositories\PersonGroups\PersonGroupsRepository;
use App\Http\Requests\PersonGroups\DeleteMultiplyRequest;
use App\Repositories\PlatformUsers\PlatformUsersRepository;

class AdminGroupController extends Controller
{
    private $personGroupsRepository;
    private $userRepository;

    const PER_PAGE = 15;

    public function __construct(PersonGroupsRepository $personGroupsRepository, PlatformUsersRepository $userRepository)
    {
        $this->personGroupsRepository = $personGroupsRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * get all groups
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/groups",
     *     operationId="getGroups",
     *     tags={"AdminGroupController"},
     *     description="get all groups",
     *     @OA\Parameter(
     *          name="page",
     *          description="Page number",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="search",
     *          description="Search expression",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getGroups(): JsonResponse
    {
        $allGroups = $this->personGroupsRepository->getAll(request('search'))->paginate(self::PER_PAGE);
        return response()->json($allGroups, 200);
    }

    /**
     * get one group
     * @param PersonGroups $group
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/groups/{group}",
     *     operationId="getGroup",
     *     tags={"AdminGroupController"},
     *     description="get one group",
     *     @OA\Parameter(
     *          name="group",
     *          description="Group id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getGroup(PersonGroups $group): JsonResponse
    {
        $group = $this->personGroupsRepository->getGroupWithStudents($group->id);

        return response()->json($group, 200);
    }

    /**
     * create group
     * @param CreateRequest $request
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/admin/groups/group/create",
     *     operationId="createGroup",
     *     tags={"AdminGroupController"},
     *     description="create group method",
     *     @OA\Parameter(
     *          name="name",
     *          description="Name group",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function createGroup(CreateRequest $request): JsonResponse
    {
        $createData = $request->validated();

        $data = $this->personGroupsRepository->createGroup($createData);

        return response()->json(['group' => $data, 'message' => 'Group created'], 200);
    }

    /**
     * update group
     * @param UpdateRequest $request
     * @param PersonGroups $group
     * @return JsonResponse
     * @OA\Put(
     *     path="/api/admin/groups/group/{group}",
     *     operationId="updateGroup",
     *     tags={"AdminGroupController"},
     *     description="update group",
     *     @OA\Parameter(
     *          name="group",
     *          description="Group id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="name",
     *          description="Group name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function editGroup(UpdateRequest $request, PersonGroups $group): JsonResponse
    {
        $updateData = $request->validated();

        $data = $this->personGroupsRepository->editGroup($group, $updateData);

        return response()->json(['group' => $data, 'message' => 'Group updated'], 200);
    }

    /**
     * get ready to attach students
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/groups/students",
     *     operationId="getReadyToAttachStudents",
     *     tags={"AdminGroupController"},
     *     description="get ready to attach students",
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function getReadyToAttachStudents()
    {
        $students = $this->userRepository->getReadyToAttachStudents();
        return response()->json($students, 200);
    }

    /**
     * assign array of students to group
     * @param AssignRequest $request
     * @param PersonGroups $group
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/admin/groups/group/{group}/assign",
     *     operationId="groups.assign",
     *     tags={"AdminGroupController"},
     *     description="assign array of students to group",
     *     @OA\Parameter(
     *          name="group",
     *          description="Group id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="Students ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function assignStudents(AssignRequest $request, PersonGroups $group): JsonResponse
    {
        $group = $this->personGroupsRepository->assignStudents($group, request('ids'));

        return response()->json(['group' => $group, 'message' => 'students assigned'], 200);
    }

    /**
     * detach array of students from group
     * @param DetachRequest $request
     * @param PersonGroups $group
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/admin/groups/group/{group}/detach",
     *     operationId="groups.detach",
     *     tags={"AdminGroupController"},
     *     description="detach array of students from group",
     *     @OA\Parameter(
     *          name="group",
     *          description="Group id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="Students ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function detachStudents(DetachRequest $request, PersonGroups $group): JsonResponse
    {
        $group = $this->personGroupsRepository->detachStudents($group, request('ids'));

        return response()->json(['group' => $group, 'message' => 'students detached'], 200);
    }

    /**
     * delete single group
     * @param PersonGroups $group
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/groups/group/{group}",
     *     operationId="groups.delete",
     *     tags={"AdminGroupController"},
     *     description="delete single group",
     *     @OA\Parameter(
     *          name="group",
     *          description="Group id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer",
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteGroup(PersonGroups $group): JsonResponse
    {
        $this->personGroupsRepository->deleteGroup($group->id);
        return response()->json(['message' => 'group deleted'], 200);
    }

    /**
     * delete array of groups
     * @param DeleteMultiplyRequest $request
     * @return JsonResponse
     * @OA\Delete(
     *     path="/api/admin/groups/group/multiply",
     *     operationId="groups.delete-multiply",
     *     tags={"AdminGroupController"},
     *     description="delete array of groups",
     *     @OA\Parameter(
     *          name="ids[]",
     *          description="Group ids array",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(),
     *       ),
     *     security={ {"bearer": {}} },
     * )
     */
    public function deleteMultiplyGroups(DeleteMultiplyRequest $request): JsonResponse
    {
        foreach (request('ids') as $groupId) {
            $this->personGroupsRepository->deleteGroup($groupId);
        }
        return response()->json(['message' => 'groups deleted'], 200);
    }
}
