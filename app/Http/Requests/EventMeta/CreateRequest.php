<?php

namespace App\Http\Requests\EventMeta;

use App\Entities\Categories\Categories;
use App\Rules\EventMeta\EndTime;
use App\Rules\EventMeta\StartTime;
use App\Rules\IsStudent;
use App\Rules\IsTeacher;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $categoryId = intval(request('category_id'));

        if($categoryId === Categories::RESERVATION){
            return auth()->user()->hasPermission('create-event_reservation');
        }

        return auth()->user()->hasPermission('create-event');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'students' => 'required|array',
            'teachers' => 'required|array',
            'students.*' => ['integer', 'exists:platform_users,id', new IsStudent],
            'teachers.*' => ['integer', 'exists:platform_users,id', new IsTeacher],
            'room_id' => 'required|integer|exists:rooms,id',
            'category_id' => 'required|integer|exists:categories,id',
            'status_id' => 'nullable|integer|exists:statuses,id',
            'start_date' => 'required|date',
            'start_time' => ['required', 'string', new StartTime],
            'end_time' => ['required', 'string', new EndTime('start_time')],
        ];

        $categoryId = intval(request('category_id'));
        if($categoryId === Categories::LESSON){
            $rules['subject_id'] = 'required|integer|exists:subjects,id';
        }else{
            $rules['event_name'] = 'required|string|max:150';
            $rules['description'] = 'nullable|string|max:1000';
        }

        return $rules;
    }
}
