<?php

namespace App\Http\Requests\EventMeta;

use App\Entities\Categories\Categories;
use App\Rules\EventMeta\EndTime;
use App\Rules\EventMeta\StartTime;
use App\Rules\IsStudent;
use App\Rules\IsTeacher;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DraftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'students' => 'required|array',
            'students.*' => ['integer', 'exists:platform_users,id', new IsStudent],
            'teachers.*' => ['integer', 'exists:platform_users,id', new IsTeacher],
            'room_id' => 'required|integer|exists:rooms,id',
            'category_id' => ['required', 'integer', Rule::in([Categories::LESSON,])],
            'status_id' => 'nullable|integer|exists:statuses,id',
            'subject_id' => 'required|integer|exists:subjects,id',
            'week_days' => 'required|array',
            'week_days.*' => 'integer|min:0|max:6',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'start_time' => ['required', 'string', new StartTime],
            'end_time' => ['required', 'string', new EndTime('start_time')],
            'interval' => 'integer|min:1|max:10',
        ];
    }
}
