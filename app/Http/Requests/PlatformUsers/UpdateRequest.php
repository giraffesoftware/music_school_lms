<?php


namespace App\Http\Requests\PlatformUsers;


use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Route::input('user');
        $status = $this->request->get('status');

        $rules = [
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('platform_users')->ignore($userId)
            ],
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'middle_name' => 'nullable|string|max:255',
            'role_ids' => 'required|array',
            'role_ids.*' => 'integer|exists:roles,id',
            'e_klass_personal_code' => [
                'required',
                'string',
                'max:255',
                Rule::unique('platform_users')->ignore($userId)
            ],
            'phone_number' => 'nullable|string|max:255',
        ];

        if (filter_var($status, FILTER_VALIDATE_BOOLEAN)) {
            $rules['status'] = 'required|boolean';
        } else {
            $rules['status'] = 'required|in:true,false';
        }

        return $rules;
    }
}
