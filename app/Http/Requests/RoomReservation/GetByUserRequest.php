<?php

namespace App\Http\Requests\RoomReservation;

use Illuminate\Foundation\Http\FormRequest;

class GetByUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = request()->route('user');

        if($user->id === auth()->id()){
            return auth()->user()->hasPermission('read-self-individual_plan');
        }
        return auth()->user()->hasPermission('read-individual_plan');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
