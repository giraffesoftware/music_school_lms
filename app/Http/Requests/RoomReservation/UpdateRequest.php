<?php

namespace App\Http\Requests\RoomReservation;

use App\Entities\Categories\Categories;
use App\Rules\IsStudent;
use App\Rules\IsTeacher;
use App\Rules\RoomReservation\EndDatetime;
use App\Rules\RoomReservation\StartDatetime;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'students' => 'required|array',
            'teachers' => 'required|array',
            'students.*' => ['integer', 'exists:platform_users,id', new IsStudent],
            'teachers.*' => ['integer', 'exists:platform_users,id', new IsTeacher],
            'room_id' => 'required|integer|exists:rooms,id',
            'category_id' => 'required|integer|exists:categories,id',
            'status_id' => 'required|integer|exists:statuses,id',
            'start_datetime' => ['required', 'date', new StartDatetime()],
            'end_datetime' => ['required', 'date', 'after:start_datetime', new EndDatetime()],
        ];

        $categoryId = intval(request('category_id'));
        if($categoryId === Categories::LESSON){
            $rules['subject_id'] = 'required|integer|exists:subjects,id';
        }else{
            $rules['event_name'] = 'required|string|max:150';
            $rules['description'] = 'nullable|string|min:3|max:1000';
        }

        return $rules;
    }
}
