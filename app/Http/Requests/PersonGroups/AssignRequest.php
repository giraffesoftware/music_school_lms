<?php


namespace App\Http\Requests\PersonGroups;


use App\Rules\HasGroup;
use App\Rules\IsStudent;
use App\Rules\DoesntHaveGroup;
use Illuminate\Foundation\Http\FormRequest;

class AssignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'array',
            'ids.*' => ['integer', 'exists:platform_users,id', new IsStudent, new DoesntHaveGroup]
        ];
    }
}
