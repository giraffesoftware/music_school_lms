<?php

namespace App\Repositories\PlatformUsers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlatformUsersRepository.
 *
 * @package namespace App\Repositories\PlatformUsers;
 */
interface PlatformUsersRepository extends RepositoryInterface
{

}
