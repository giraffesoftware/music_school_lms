<?php

namespace App\Repositories\PlatformUsers;

use App\Role;
use App\Criteria\StudentCriteria;
use App\Criteria\TeacherCriteria;
use App\Criteria\WithTrashedCriteria;
use App\Presenters\PlatformUsersPresenter;
use App\Criteria\ReadyToAttachStudentCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\PlatformUsers\PlatformUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

/**
 * Class PlatformUsersRepositoryEloquent.
 *
 * @package namespace App\Repositories\PlatformUsers;
 */
class PlatformUsersRepositoryEloquent extends BaseRepository implements PlatformUsersRepository
{
    use SendsPasswordResetEmails;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlatformUsers::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->setPresenter(PlatformUsersPresenter::class);
    }


    public function getStudents()
    {
        $this->pushCriteria(StudentCriteria::class);
        return $this->all();
    }

    public function getTeachers()
    {
        $this->pushCriteria(TeacherCriteria::class);
        return $this->all();
    }

    public function getReadyToAttachStudents()
    {
        $this->pushCriteria(ReadyToAttachStudentCriteria::class);
        return $this->all();
    }

    public static function isTeacher($userId)
    {
        return PlatformUsers::withTrashed()->find($userId)->hasRole('teacher');
    }

    public static function isStudent($userId)
    {
        return PlatformUsers::withTrashed()->find($userId)->hasRole('student');
    }

    public function getAll($searchQuery = null, $roleId = null)
    {
        $searchQuery = $searchQuery ?? '';

        return $this->scopeQuery(function ($query) use ($searchQuery, $roleId) {

            return $query->join('role_user', 'platform_users.id', '=', 'role_user.user_id')
                ->when($roleId, function($query, $roleId) {
                    return $query->where('role_user.role_id', $roleId);
                })->whereLike([
                    'name', 'surname', 'middle_name',
                    'email', 'phone_number', 'e_klass_personal_code'], $searchQuery
                )->orWhere('id', $searchQuery);

        });
    }

    /**
     * @param array $rolesIds
     * @return array
     */
    public function getUpdateUserErrors(array $rolesIds): array
    {
        $errors = [];

        if (in_array(Role::studentRole()->id, $rolesIds) && count($rolesIds) > 1) {
            $errors['role_ids'] = __('errors.role_student_must_be_one');
        }

        return $errors;
    }

    /**
     * @param PlatformUsers $user
     * @param array $updateData
     * @return array
     */
    public function editUser(PlatformUsers $user, array $updateData): array
    {
        $user->detachRoles($user->user_roles);

        $user->update($updateData);

        $user->attachRoles($updateData['role_ids']);

        if (!filter_var($updateData['status'], FILTER_VALIDATE_BOOLEAN)) {
            $user->delete();
        } else if ($user->trashed()) {
            $user->restore();
        }

        return $this->pushCriteria(WithTrashedCriteria::class)->find($user->id);
    }

    /**
     * @param PlatformUsers $user
     * @param boolean|string $forceDelete
     * @param int $authUserId
     * @return array
     */
    public function getDeleteUserErrors(PlatformUsers $user, $forceDelete, int $authUserId): array
    {
        $errors = [];

        if ($user->id === $authUserId) {
            $errors['user'] = __('errors.current_user_cannot_delete_himself');
        }

        if (!filter_var($forceDelete, FILTER_VALIDATE_BOOLEAN) || $user->trashed()) {
            if ($user->attendings->count() > 0) {
                $errors['event'] = __('errors.user_is_participant');
            }
        }

        return $errors;
    }

    /**
     * @param array $userIds
     * @param boolean|string $forceDelete
     * @param int $authUserId
     * @return array
     */
    public function getMultiplyDeleteUserErrors(array $userIds, $forceDelete, int $authUserId): array
    {
        $errors = [];

        $this->skipPresenter()->pushCriteria(WithTrashedCriteria::class);
        $users = $this->findWhereIn('id', $userIds);
        foreach ($users as $user) {
            if ($user->id === $authUserId) {
                $errors['user'] = __('errors.current_user_cannot_delete_himself');
            }

            if (!filter_var($forceDelete, FILTER_VALIDATE_BOOLEAN) || $user->trashed()) {
                if ($user->attendings->count() > 0) {
                    $errors['event']['message'] = __('errors.users_is_participant');
                    if (PlatformUsersRepositoryEloquent::isStudent($user->id)) {
                        $errors['event']['students'][] = $user->name;
                    } else if (PlatformUsersRepositoryEloquent::isTeacher($user->id)) {
                        $errors['event']['teachers'][] = $user->name;
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * @param int $userId
     */
    public function deleteUser(int $userId)
    {
        //Move user to archive
        $this->skipPresenter();
        $user = $this->pushCriteria(WithTrashedCriteria::class)->find($userId);

        if ($user->trashed()) {
            $user->forceDelete();
        } else {
            $user->delete();
        }
    }

    /**
     * @param array $createData
     * @return array
     */
    public function createUser(array $createData): array
    {
        $result = $this->create($createData);

        $user = PlatformUsers::query()->find($result['data']['id']);

        $user->attachRoles($createData['role_ids']);

        if (!filter_var($createData['status'], FILTER_VALIDATE_BOOLEAN)) {
            $user->delete();
        }

        $this->broker()->sendResetLink(['email' => $user->email]);

        return $this->pushCriteria(WithTrashedCriteria::class)->find($user->id);
    }
}
