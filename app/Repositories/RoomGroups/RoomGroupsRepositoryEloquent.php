<?php

namespace App\Repositories\RoomGroups;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RoomGroups\RoomGroupsRepository;
use App\Entities\RoomGroups\RoomGroups;
use App\Validators\RoomGroups\RoomGroupsValidator;

/**
 * Class RoomGroupsRepositoryEloquent.
 *
 * @package namespace App\Repositories\RoomGroups;
 */
class RoomGroupsRepositoryEloquent extends BaseRepository implements RoomGroupsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomGroups::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    public function getAllGroupRoomsWithRooms()
    {
        return $this->model->with('rooms')->get();
    }
}
