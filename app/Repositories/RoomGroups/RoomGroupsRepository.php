<?php

namespace App\Repositories\RoomGroups;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomGroupsRepository.
 *
 * @package namespace App\Repositories\RoomGroups;
 */
interface RoomGroupsRepository extends RepositoryInterface
{
    //
}
