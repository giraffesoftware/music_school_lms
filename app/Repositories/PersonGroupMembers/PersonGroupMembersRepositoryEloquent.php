<?php

namespace App\Repositories\PersonGroupMembers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PersonGroupMembers\PersonGroupMembersRepository;
use App\Entities\PersonGroupMembers\PersonGroupMembers;
use App\Validators\PersonGroupMembers\PersonGroupMembersValidator;

/**
 * Class PersonGroupMembersRepositoryEloquent.
 *
 * @package namespace App\Repositories\PersonGroupMembers;
 */
class PersonGroupMembersRepositoryEloquent extends BaseRepository implements PersonGroupMembersRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PersonGroupMembers::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
