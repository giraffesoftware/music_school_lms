<?php

namespace App\Repositories\PersonGroupMembers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PersonGroupMembersRepository.
 *
 * @package namespace App\Repositories\PersonGroupMembers;
 */
interface PersonGroupMembersRepository extends RepositoryInterface
{
    //
}
