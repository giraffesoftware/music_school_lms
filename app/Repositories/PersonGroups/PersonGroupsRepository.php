<?php

namespace App\Repositories\PersonGroups;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PersonGroupsRepository.
 *
 * @package namespace App\Repositories\PersonGroups;
 */
interface PersonGroupsRepository extends RepositoryInterface
{
    //
}
