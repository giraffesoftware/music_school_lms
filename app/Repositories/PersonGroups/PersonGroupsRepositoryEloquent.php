<?php

namespace App\Repositories\PersonGroups;

use App\Presenters\PersonGroupsPresenter;
use App\Entities\PlatformUsers\PlatformUsers;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PersonGroups\PersonGroupsRepository;
use App\Entities\PersonGroups\PersonGroups;
use App\Validators\PersonGroups\PersonGroupsValidator;

/**
 * Class PersonGroupsRepositoryEloquent.
 *
 * @package namespace App\Repositories\PersonGroups;
 */
class PersonGroupsRepositoryEloquent extends BaseRepository implements PersonGroupsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PersonGroups::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    public function getGroupsWithStudents()
    {
        $this->setPresenter(PersonGroupsPresenter::class);
        return $this->with('platform_students')->all();
    }

    public function getAll($searchQuery = null)
    {
        $searchQuery = $searchQuery ?? '';

        $this->setPresenter(PersonGroupsPresenter::class);

        return $this->scopeQuery(function ($query) use ($searchQuery) {
            return $query->whereHas('platform_students', function ($query) use ($searchQuery) {
                $query->where('name', 'LIKE', "%{$searchQuery}%")
                    ->orWhere('surname', 'LIKE', "%{$searchQuery}%")
                    ->orWhere('middle_name', 'LIKE', "%{$searchQuery}%");
            })->orWhere('name', 'LIKE', "%{$searchQuery}%");
        });
    }

    public function getGroupWithStudents(int $groupId)
    {
        $this->setPresenter(PersonGroupsPresenter::class);
        return $this->with('platform_students')->find($groupId);
    }

    /**
     * @param array $createData
     * @return array
     */
    public function createGroup(array $createData): array
    {
        $result = $this->create($createData);

        $this->setPresenter(PersonGroupsPresenter::class);

        return $this->find($result->id);
    }

    /**
     * @param PersonGroups $group
     * @param array $updateData
     * @return array
     */
    public function editGroup(PersonGroups $group, array $updateData): array
    {
        $group->update($updateData);

        $this->setPresenter(PersonGroupsPresenter::class);

        return $this->find($group->id);
    }

    /**
     * @param PersonGroups $group
     * @param array $studentIds
     * @return array
     */
    public function assignStudents(PersonGroups $group, array $studentIds): array
    {
        $students = PlatformUsers::whereIn('id', $studentIds);
        $students->update(['person_group_id' => $group->id]);

        $this->setPresenter(PersonGroupsPresenter::class);

        return $this->find($group->id);
    }

    /**
     * @param PersonGroups $group
     * @param array $studentIds
     * @return array
     */
    public function detachStudents(PersonGroups $group, array $studentIds): array
    {
        $students = PlatformUsers::where('person_group_id', $group->id)->whereIn('id', $studentIds);
        $students->update(['person_group_id' => null]);

        $this->setPresenter(PersonGroupsPresenter::class);

        return $this->find($group->id);
    }

    /**
     * @param int $groupId
     */
    public function deleteGroup(int $groupId)
    {
        $group = $this->find($groupId);
        $group->delete();
    }
}
