<?php

namespace App\Repositories\RoomReservations;

use App\Entities\Attendings\Attendings;
use App\Entities\Categories\Categories;
use App\Entities\PlatformUsers\PlatformUsers;
use App\Entities\Status\Status;
use App\Presenters\RoomReservationsPresenter;
use App\Repositories\EventMeta\EventMetaRepository;
use Carbon\Carbon;
use Illuminate\Container\Container as Application;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\RoomReservations\RoomReservations;

/**
 * Class RoomReservationsRepositoryEloquent.
 *
 * @package namespace App\Repositories\RoomReservations;
 */
class RoomReservationsRepositoryEloquent extends BaseRepository implements RoomReservationsRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomReservations::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->setPresenter(RoomReservationsPresenter::class);
    }


    public function storeReservation(array $reservationData)
    {
        $this->skipPresenter();
        $data = [
            'room_id' => $reservationData['room_id'],
            'start_datetime' => $reservationData['start_datetime'],
            'end_datetime' => $reservationData['end_datetime'],

            'category_id' => $reservationData['category_id'],
            'event_meta_id' => $reservationData['event_meta_id'],
            'status_id' => $reservationData['status_id'] ?? Status::BOOKED,
        ];

        $categoryId = intval($reservationData['category_id']);
        if ($categoryId === Categories::LESSON) {
            if (empty($reservationData['subject_id'])) {
                throw new \Exception('subject_id is required', 400);
            }

            $data['subject_id'] = $reservationData['subject_id'];
        } else {
            if (empty($reservationData['event_name'])) {
                throw new \Exception('name is required', 400);
            }

            $data['event_name'] = $reservationData['event_name'];
            $data['description'] = $reservationData['description'] ?? null;
        }

        return $this->create($data);
    }

    public function undraftReservation(int $id)
    {
        $roomReservation = $this->model->find($id);

        if (empty($this->getReservationErrors($roomReservation))) {
            $roomReservation->is_draft = false;
            return $roomReservation->save();
        }

        return false;
    }

    /**
     * @param RoomReservations $roomReservation
     * @return array
     */
    public function getReservationErrors(RoomReservations $roomReservation): array
    {
        $usersIds = $roomReservation->attendings()->pluck('user_id')->toArray();

        return $this->getReservationDataErrors($usersIds, $roomReservation->room_id,
            $roomReservation->start_datetime, $roomReservation->end_datetime, [$roomReservation->id],
            $roomReservation->event_meta_id);
    }

    /**
     * @param array $usersIds
     * @param int $roomId
     * @param Carbon $startDatetime
     * @param Carbon $endDatetime
     * @param array $exceptReservationIds
     * @param null $eventMetaId
     * @return array
     */
    public function getReservationDataErrors(array $usersIds, int $roomId, Carbon $startDatetime, Carbon $endDatetime,
                                             array $exceptReservationIds = [], ?int $eventMetaId = null): array
    {
        $errors = [];

        $conflictedUsersReservations = $this->getReservationsByUsersIdsAndDatetime($usersIds,
            $startDatetime, $endDatetime, $exceptReservationIds, $eventMetaId);
        $conflictedRoomReservations = $this->getReservationsByRoomIdAndDatetime($roomId,
            $startDatetime, $endDatetime, $exceptReservationIds, $eventMetaId);

        $conflictedUsersReservations->each(function (RoomReservations $roomReservation) use ($usersIds, &$errors) {
            $roomReservation->teachers()->whereIn('user_id', $usersIds)->get()
                ->each(function ($teacher) use (&$errors) {
                    $errors['teachers'][$teacher->id] = __('errors.teacher_already_has_reservation',
                        ['name' => $teacher->username]);
                });
            $roomReservation->students()->whereIn('user_id', $usersIds)->get()
                ->each(function ($student) use (&$errors) {
                    $errors['students'][$student->id] = __('errors.student_already_has_reservation',
                        ['name' => $student->username]);
                });
        });

        if ($conflictedRoomReservations->count()) {
            $errors['room'] = __('errors.room_is_already_reserved',
                ['number' => $conflictedRoomReservations->first()->room->room_number]);
        }

        return $errors;
    }


    public function getAll()
    {
        $reservations = $this->with(['subject', 'category', 'event_meta', 'room.room_group'])->all();

        return $reservations;
    }

    public function getReservation(int $reservationId)
    {
        $reservation = $this->with(['teachers', 'students'])->findWhere(['id' => $reservationId]);

        return $reservation;
    }

    public function getReservationByDate(string $dateFrom, string $dateTo)
    {
        return $this->findWhere([
            'is_draft' => false,
            ['start_datetime', '>=', $dateFrom],
            ['start_datetime', '<=', $dateTo],
        ]);
    }

    public function getReservationByUserAndDate(PlatformUsers $user, Carbon $dateFrom, Carbon $dateTo)
    {
        $reservations = $this->whereHas('attendings', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->findWhere([
            'is_draft' => false,
            ['start_datetime', '>=', $dateFrom],
            ['start_datetime', '<=', $dateTo],
        ]);;


        return $reservations;
    }

    public function getReservationsByRoomIdAndDatetime(int $roomId, Carbon $startTime, Carbon $endTime,
                                                       array $exceptReservationIds = [],
                                                       ?int $includeReservationsFromEventMetaId = null)
    {
        $query = $this->model::where('room_id', $roomId)
            ->whereNotIn('id', $exceptReservationIds)
            ->where('start_datetime', '<', $endTime)
            ->where('end_datetime', '>', $startTime);

        if ($includeReservationsFromEventMetaId) {
            $query->where(function ($q) use ($includeReservationsFromEventMetaId) {
                $q->where('is_draft', false)->orWhere('event_meta_id', $includeReservationsFromEventMetaId);
            });
        } else {
            $query->where('is_draft', false);
        }

        return $query->get();
    }

    public function getReservationsByUsersIdsAndDatetime(array $userIds, Carbon $startTime, Carbon $endTime,
                                                         array $exceptReservationIds = [],
                                                         ?int $includeReservationsFromEventMetaId = null)
    {
        $query = $this->model::whereHas(
            'attendings', function ($query) use ($userIds) {
            $query->whereIn('user_id', $userIds);
        })
            ->whereNotIn('id', $exceptReservationIds)
            ->where('start_datetime', '<', $endTime)
            ->where('end_datetime', '>', $startTime);

        if ($includeReservationsFromEventMetaId) {
            $query->where(function ($q) use ($includeReservationsFromEventMetaId) {
                $q->where('is_draft', false)->orWhere('event_meta_id', $includeReservationsFromEventMetaId);
            });
        } else {
            $query->where('is_draft', false);
        }

        return $query->get();
    }

    public function deleteReservation(int $reservationId)
    {
        $this->skipPresenter();
        $reservation = $this->find($reservationId);
        $reservation->delete();
    }

    public function editReservation(RoomReservations $roomReservation, $updateData)
    {
        $roomReservation->deleteAllEventAttendings();
        $users = array_merge($updateData['teachers'], $updateData['students']);
        unset($updateData['teachers'], $updateData['students']);
        $attendings = [];
        foreach ($users as $user) {
            $attendings[] = new Attendings(['user_id' => $user]);
        }
        $roomReservation->attendings()->saveMany($attendings);
        $data = [
            'room_id' => $updateData['room_id'],
            'start_datetime' => Carbon::parse($updateData['start_datetime']),
            'end_datetime' => Carbon::parse($updateData['end_datetime']),

            'category_id' => $updateData['category_id'],
            'status_id' => $updateData['status_id'],
        ];

        $categoryId = intval($updateData['category_id']);
        if ($categoryId === Categories::LESSON) {
            if (empty($updateData['subject_id'])) {
                throw new \Exception('subject_id is required', 400);
            }

            $data['subject_id'] = $updateData['subject_id'];
        } else {
            if (empty($updateData['event_name'])) {
                throw new \Exception('name is required', 400);
            }

            $data['event_name'] = $updateData['event_name'];
            $data['description'] = $updateData['description'] ?? null;
        }
        $roomReservation->update($data);

        return $this->find($roomReservation->id);
    }

    public function deleteOldDraftRoomReservations()
    {
        $this->skipPresenter();
        $this->model->where('created_at', '<', Carbon::now()->subWeek())->delete();
    }
}
