<?php

namespace App\Repositories\RoomReservations;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomReservationsRepository.
 *
 * @package namespace App\Repositories\RoomReservations;
 */
interface RoomReservationsRepository extends RepositoryInterface
{
    //
}
