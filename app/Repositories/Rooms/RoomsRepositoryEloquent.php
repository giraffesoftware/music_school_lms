<?php

namespace App\Repositories\Rooms;

use App\Presenters\RoomSchedulePresenter;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Rooms\RoomsRepository;
use App\Entities\Rooms\Rooms;
use App\Validators\Rooms\RoomsValidator;

/**
 * Class RoomsRepositoryEloquent.
 *
 * @package namespace App\Repositories\Rooms;
 */
class RoomsRepositoryEloquent extends BaseRepository implements RoomsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Rooms::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @param array $roomIds
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     * @return mixed
     */
    public function getRoomsSchedule(array $roomIds, Carbon $dateFrom, Carbon $dateTo)
    {
        $this->setPresenter(RoomSchedulePresenter::class);
        $roomSchedule = $this->with(['room_group', 'room_reservations.room','room_reservations.subject',
            'room_reservations.category','room_reservations.event_meta','room_reservations.teachers',
            'room_reservations' => function ($q) use ($dateFrom, $dateTo) {
                $q->whereBetween('start_datetime', [$dateFrom, $dateTo])->where('is_draft', false);
            },
        ])->findWhereIn('id',$roomIds);

        return $roomSchedule;
    }
}
