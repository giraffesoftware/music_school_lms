<?php

namespace App\Repositories\Rooms;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomsRepository.
 *
 * @package namespace App\Repositories\Rooms;
 */
interface RoomsRepository extends RepositoryInterface
{
    //
}
