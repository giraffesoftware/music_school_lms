<?php

namespace App\Repositories\EventMeta;

use App\Presenters\EventMetaPresenter;
use App\Repositories\RoomReservations\RoomReservationsRepository;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\EventMeta\EventMeta;

/**
 * Class EventMetaRepositoryEloquent.
 *
 * @package namespace App\Repositories\EventMeta;
 */
class EventMetaRepositoryEloquent extends BaseRepository implements EventMetaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EventMeta::class;
    }

    public function storeEventMeta(array $eventData, bool $isSingle = true)
    {
        $this->skipPresenter();
        $data = [
            'is_single' => $isSingle,
            'week_days' => $eventData['week_days'] ?? null,
            'interval' => $eventData['interval'] ?? 1,
            'start_date' => Carbon::parse($eventData['start_date']),
            'end_date' => Carbon::parse($eventData['end_date']),
            'start_time' => $eventData['start_time'],
            'end_time' => $eventData['end_time'],
        ];
        return $this->create($data);
    }


    public function deleteEventMeta(int $eventMetaId)
    {
        $this->skipPresenter();

        $eventMeta = $this->find($eventMetaId);
        $eventMeta->delete();
    }


    public function getEventMeta(int $eventMetaId)
    {
        $eventMeta = $this->with(['room_reservations.teachers','room_reservations.students'])
            ->findWhere(['id' => $eventMetaId]);

        return $eventMeta;
    }

    public function undraftEventMetaTry(EventMeta $eventMeta)
    {
        $roomReservationsRepository = resolve(RoomReservationsRepository::class);

        //undraft all reservations if all of them are correct
        if($eventMeta->room_reservations->every(function ($roomReservation) use($roomReservationsRepository){
            return empty($roomReservationsRepository->getReservationErrors($roomReservation));
        })){
            $eventMeta->room_reservations->each(function ($roomReservation){
                $roomReservation->is_draft = false;
                $roomReservation->save();
            });

            return true;
        }

        return false;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->setPresenter(EventMetaPresenter::class);
    }

}
