<?php

namespace App\Repositories\EventMeta;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EventMetaRepository.
 *
 * @package namespace App\Repositories\EventMeta;
 */
interface EventMetaRepository extends RepositoryInterface
{
    //
}
