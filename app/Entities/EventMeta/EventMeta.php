<?php

namespace App\Entities\EventMeta;

use App\Entities\RoomReservations\RoomReservations;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EventMeta.
 *
 * @property int $id
 * @property bool $is_single
 * @property array $week_days
 * @property int $interval
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string $start_time
 * @property string $end_time
 * @property-read Collection|RoomReservations[] $room_reservations
 *
 * @package namespace App\Entities\EventMeta;
 */
class EventMeta extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'event_meta';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_meta', 'is_single', 'week_days', 'start_date',
        'end_date', 'start_time', 'end_time', 'interval'
    ];

    protected $casts = [
        'week_days' => 'array',
    ];
    protected $dates = [
        'start_date',
        'end_date',
    ];

    public $timestamps = false;


    public function room_reservations()
    {
        return $this->hasMany(RoomReservations::class);
    }
}
