<?php

namespace App\Entities\PersonGroups;

use App\Entities\PlatformUsers\PlatformUsers;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PersonGroups.
 *
 * @package namespace App\Entities\PersonGroups;
 */
class PersonGroups extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public $timestamps = false;

    public function platform_students()
    {
        return $this->hasMany(PlatformUsers::class, 'person_group_id')->students();
    }

}
