<?php

namespace App\Entities\RoomGroups;

use App\Entities\Rooms\Rooms;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RoomGroups.
 *
 * @package namespace App\Entities\RoomGroups;
 */
class RoomGroups extends Model implements Transformable
{
    use TransformableTrait;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    public function rooms()
    {
        return $this->hasMany(Rooms::class,'room_group_id','id');
    }
}
