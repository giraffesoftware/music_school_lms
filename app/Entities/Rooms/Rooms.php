<?php

namespace App\Entities\Rooms;

use App\Entities\RoomGroups\RoomGroups;
use App\Entities\RoomReservations\RoomReservations;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Rooms.
 *
 * @package namespace App\Entities\Rooms;
 */
class Rooms extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public $timestamps = false;

    public function room_group()
    {
        return $this->belongsTo(RoomGroups::class);
    }

    public function room_reservations()
    {
        return $this->hasMany(RoomReservations::class,'room_id','id');
    }

}
