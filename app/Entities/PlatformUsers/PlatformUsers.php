<?php

namespace App\Entities\PlatformUsers;

use App\Role;
use App\Entities\Attendings\Attendings;
use App\Entities\PersonGroups\PersonGroups;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\RoomReservations\RoomReservations;
use App\Notifications\MailResetPasswordNotification;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use \Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Laratrust\Traits\LaratrustUserTrait;


/**
 * Class PlatformUsers.
 *
 * @package namespace App\Entities\PlatformUsers;
 */
class PlatformUsers extends Model implements Transformable, JWTSubject, Authenticatable, CanResetPassword
{
    use TransformableTrait,
        AuthenticableTrait,
        CanResetPasswordTrait,
        Notifiable,
        LaratrustUserTrait,
        SoftDeletes;

    const  TEACHER = 1;
    const  STUDENT = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'surname', 'middle_name',
        'e_klass_id', 'phone_number', 'e_klass_personal_code', 'person_group_id'];

    public $timestamps = false;

    public function getFullNameAttribute()
    {
        // Remove space if middle name is empty
        return trim($this->attributes['name'] . ' ' . $this->attributes['middle_name']) . ' ' . $this->attributes['surname'];
    }

    public function person_group()
    {
        return $this->belongsTo(PersonGroups::class, 'person_group_id');
    }

    public function reservations()
    {
        return $this->belongsToMany(RoomReservations::class, 'attendings', 'user_id', 'reservation_id');
    }

    public function attendings()
    {
        return $this->hasMany(Attendings::class, 'user_id', 'id');
    }

    public function user_roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }

    public function scopeTeachers($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', 'teacher');
        });
    }

    public function scopeStudents($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', 'student');
        });
    }
}
