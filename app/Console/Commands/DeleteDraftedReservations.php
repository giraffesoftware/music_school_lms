<?php

namespace App\Console\Commands;

use App\Repositories\RoomReservations\RoomReservationsRepository;
use Illuminate\Console\Command;

class DeleteDraftedReservations extends Command
{
    private $roomReservationsRepository;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-reservations:delete-drafted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes old drafted room reservations';

    /**
     * Create a new command instance.
     *
     * @param RoomReservationsRepository $roomReservationsRepository
     */
    public function __construct(RoomReservationsRepository $roomReservationsRepository)
    {
        parent::__construct();
        $this->roomReservationsRepository = $roomReservationsRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->roomReservationsRepository->deleteOldDraftRoomReservations();
    }
}
