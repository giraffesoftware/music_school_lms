<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RoomReservationsValidatorValidator.
 *
 * @package namespace App\Validators;
 */
class RoomReservationsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'event_name' => 'required',
            'room_id'  => 'required|exists:rooms,id',
            'date_reservation'=> 'required|date_format:Y-m-d',
            'time_from'=> 'required|date_format:H:i',
            'time_to'=> 'required|date_format:H:i',
            'category_id'=> 'required|exists:categories,id',
            'subject_id'=> 'required|exists:subjects,id',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];

    protected $attributes = [
        'event_name' => 'Event name',
        'room_id' => 'Room id',
        'date_reservation' => 'Date reservation',
        'time_from' => 'Time from',
        'time_to' => 'Time to',
        'category_id' => 'Category id',
        'subject_id' => 'Subject id',
    ];

    protected $messages = [
        'required' => 'The :attribute field is required.',
    ];
}
