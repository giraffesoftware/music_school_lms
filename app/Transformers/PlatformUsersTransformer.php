<?php

namespace App\Transformers;

use App\Entities\PlatformUsers\PlatformUsers;
use League\Fractal\TransformerAbstract;

/**
 * Class PlatformUsersPresenterTransformer.
 *
 * @package namespace App\Transformers;
 */
class PlatformUsersTransformer extends TransformerAbstract
{
    /**
     * Transform the PlatformUsersPresenter entity.
     *
     * @param PlatformUsers $model
     *
     * @return array
     */
    public function transform(PlatformUsers $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'middle_name' => $model->middle_name,
            'surname' => $model->surname,
            'full_name' => $model->full_name,
            'email' => $model->email,
            'roles' => $this->transformRoles($model->user_roles),
            'phone_number' => $model->phone_number,
            'e_klass_personal_code' => $model->e_klass_personal_code,
            'status' => $this->transformStatus($model)
        ];
    }

    private function transformRoles($roles)
    {
        return $roles->map(function ($item) {
            return (new RoleTransformer)->transform($item);
        })->toArray();
    }

    private function transformStatus($model){
        return $model->trashed() ? 'archived' : 'active';
    }
}
