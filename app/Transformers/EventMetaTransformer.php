<?php

namespace App\Transformers;

use App\Entities\RoomReservations\RoomReservations;
use App\Entities\EventMeta\EventMeta;
use League\Fractal\TransformerAbstract;

/**
 * Class RoomReservationsTransformer.
 *
 * @package namespace App\Transformers;
 */
class EventMetaTransformer extends TransformerAbstract
{
    /**
     * Transform the RoomReservations entity.
     *
     * @param EventMeta $model
     *
     * @return array
     */
    public function transform(EventMeta $model)
    {
        return [
            'id'         => (int) $model->id,
            'room_reservations' => $this->transformRoomReservations($model->room_reservations)
        ];
    }

    private function transformRoomReservations($roomReservations)
    {
        return $roomReservations->map(function ($item) {
            return (new RoomReservationsTransformer())->transform($item);
        })->toArray();
    }
}
