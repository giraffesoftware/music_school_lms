<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\RoomReservationsByRoomIds;

/**
 * Class RoomReservationsByRoomIdsTransformer.
 *
 * @package namespace App\Transformers;
 */
class RoomReservationsByRoomIdsTransformer extends TransformerAbstract
{
    /**
     * Transform the RoomReservationsByRoomIds entity.
     *
     * @param \App\Entities\RoomReservationsByRoomIds $model
     *
     * @return array
     */
    public function transform(RoomReservationsByRoomIds $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
