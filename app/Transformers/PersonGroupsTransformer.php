<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PersonGroups\PersonGroups;

/**
 * Class PersonGroupsTransformer.
 *
 * @package namespace App\Transformers;
 */
class PersonGroupsTransformer extends TransformerAbstract
{
    /**
     * Transform the PersonGroups entity.
     *
     * @param PersonGroups $model
     *
     * @return array
     */
    public function transform(PersonGroups $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'         =>  $model->name,
            "group_students" => $this->transformStudents($model->platform_students)

        ];
    }

    private function transformStudents(\Illuminate\Support\Collection $students)
    {
        return $students->map(function ($item) {
            return (new PlatformUsersTransformer())->transform($item);
        })->toArray();
    }
}
