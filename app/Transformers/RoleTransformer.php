<?php

namespace App\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

/**
 * Class RoleTransformer.
 *
 * @package namespace App\Transformers;
 */
class RoleTransformer extends TransformerAbstract
{
    /**
     * Transform the Role entity.
     *
     * @param Role $model
     *
     * @return array
     */
    public function transform(Role $model)
    {
        return [
            'id'    => (int)$model->id,
            'name'  => $model->name,
            'title' => $model->display_name,
        ];
    }
}
