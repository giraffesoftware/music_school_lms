<?php

namespace App\Transformers;

use App\Entities\Categories\Categories;
use App\Entities\RoomReservations\RoomReservations;
use App\Repositories\RoomReservations\RoomReservationsRepository;
use League\Fractal\TransformerAbstract;

/**
 * Class RoomReservationsTransformer.
 *
 * @package namespace App\Transformers;
 */
class RoomReservationsTransformer extends TransformerAbstract
{
    private $roomReservationsRepository;

    public function __construct()
    {
        $this->roomReservationsRepository = resolve(RoomReservationsRepository::class);
    }

    /**
     * Transform the RoomReservations entity.
     *
     * @param RoomReservations $model
     *
     * @return array
     */
    public function transform(RoomReservations $model)
    {
        $result = [
            'id' => (int)$model->id,
            'start_datetime' => $model->start_datetime->toDateTimeString(),
            'end_datetime' => $model->end_datetime->toDateTimeString(),
            'is_draft' => $model->is_draft,
            'category' => [
                'id' => $model->category->id,
                'name' => $model->category->name
            ],
            'status' => [
                'id' => $model->status->id,
                'name' => $model->status->name,
                'color' => $model->status->color,
            ],
            'subject' => empty($model->subject)?null:[
                'id' => $model->subject->id,
                'name' => $model->subject->name,
            ],
            'event_meta' => [
                'id' => $model->event_meta->id,
            ],
            'room' => [
                'id' => $model->room->id,
                'room_number' => $model->room->room_number,
                'description' => $model->room->description,
                'room_group' => $model->room->room_group->name,
            ],
            'teachers' => $this->transformUsers($model->withTrashedTeachers),
            'students' => $this->transformUsers($model->withTrashedStudents),
            'errors' => $model->is_draft ? $this->roomReservationsRepository->getReservationErrors($model) : [],
        ];

        if( $model->category->id === Categories::LESSON ){
            $result['subject'] = [
                'id' => $model->subject->id,
                'name' => $model->subject->name,
            ];
        }else{
            $result['event_name'] = $model->event_name;
            $result['description'] = $model->description;
        }
        return $result;
    }

    private function transformUsers($users)
    {
        return $users->map(function ($item) {
            return (new PlatformUsersTransformer)->transform($item);
        })->toArray();
    }
}
