<?php

namespace App\Transformers;

use App\Entities\RoomReservations\RoomReservations;
use App\Entities\Rooms\Rooms;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;

/**
 * Class RoomScheduleTransformer.
 *
 * @package namespace App\Transformers;
 */
class RoomScheduleTransformer extends TransformerAbstract
{
    /**
     * Transform the RoomSchedule entity.
     *
     * @param Rooms $model
     *
     * @return array
     */
    public function transform(Rooms $model)
    {
        return [
            'id' => (int)$model->id,
            'room_number' => $model->room_number,
            'description' => $model->description,
            'room_group' => $model->room_group->name,
            'room_reservations' => $this->transformReservations($model->room_reservations)
        ];
    }

    private function transformReservations(\Illuminate\Support\Collection $reservations)
    {
        return $reservations->map(function ($item) {
             return (new RoomReservationsTransformer)->transform($item);
        })->toArray();
    }
}
