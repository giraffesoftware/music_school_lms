<?php

namespace App\Rules;

use App\Entities\PlatformUsers\PlatformUsers;
use Illuminate\Contracts\Validation\Rule;

class DoesntHaveGroup implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !PlatformUsers::find($value)->person_group;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.group.has-group');
    }
}
