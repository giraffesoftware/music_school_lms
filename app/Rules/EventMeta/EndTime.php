<?php

namespace App\Rules\EventMeta;

use Illuminate\Contracts\Validation\Rule;

class EndTime implements Rule
{
    private $startTimeField;

    /**
     * Create a new rule instance.
     *
     * @param $startTime
     */
    public function __construct($startTimeField)
    {
        $this->startTimeField = $startTimeField;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $dateStart = \DateTime::createFromFormat('H:i', request($this->startTimeField));
        $dateEnd = \DateTime::createFromFormat('H:i', $value);
        $maxDateEnd = \DateTime::createFromFormat('H:i', '21:00');


        return $dateEnd > $dateStart && $dateEnd <= $maxDateEnd;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.reservations-time.end-time');
    }
}
