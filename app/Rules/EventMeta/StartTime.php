<?php

namespace App\Rules\EventMeta;

use Illuminate\Contracts\Validation\Rule;

class StartTime implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = \DateTime::createFromFormat('H:i', $value);

        $hours = intval($date->format('H'));

        return $hours >= 7;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.reservations-time.start-time');
    }
}
