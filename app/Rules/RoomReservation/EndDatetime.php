<?php

namespace App\Rules\RoomReservation;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class EndDatetime implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $requestDatetime = Carbon::parse($value);
        $maxTime = $requestDatetime->copy()->setTime(21, 0);
        $minTime = $requestDatetime->copy()->setTime(7, 0);
        return $requestDatetime->lte($maxTime) && $requestDatetime->gt($minTime);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.reservations-time.end-time');
    }
}
