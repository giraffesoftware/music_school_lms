<?php

namespace App;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public static function teacherRole()
    {
        return self::where('name', 'teacher')->first();
    }
    public static function studentRole()
    {
        return self::where('name', 'student')->first();
    }
}
