<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\PlatformUsers\PlatformUsers;
use Faker\Generator as Faker;

$factory->define(PlatformUsers::class, function (Faker $faker) {
    return [
        "name" => $faker->firstName,
        "surname" => $faker->lastName,
        "email" => $faker->email,
        "password" => $faker->password
    ];
});
