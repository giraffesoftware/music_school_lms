<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Entities\Rooms\Rooms::class, function (Faker $faker) {
    return [
        'room_group_id' => factory(\App\Entities\RoomGroups\RoomGroups::class)->create()->id,
        'room_number' => $faker->randomNumber(3),
        'description' => $faker->text,
    ];
});
