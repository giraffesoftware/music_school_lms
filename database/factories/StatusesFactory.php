<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Entities\Status\Status::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'color' => $faker->colorName
    ];
});
