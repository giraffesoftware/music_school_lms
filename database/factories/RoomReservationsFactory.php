<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Entities\RoomReservations\RoomReservations::class, function (Faker $faker) {
    $startDatetime = $faker->dateTime();

    return [
        'event_name' => $faker->sentence(rand(6,10)),
        'room_id' => factory(\App\Entities\Rooms\Rooms::class)->create()->id,
        'start_datetime' => $startDatetime,
        'end_datetime' => $startDatetime->modify('+2 hour'),
        'category_id' => factory(App\Entities\Categories\Categories::class)->create()->id,
        'subject_id' => factory(App\Entities\Subjects\Subjects::class)->create()->id,
        'event_meta_id' => factory(\App\Entities\EventMeta\EventMeta::class)->create()->id,
        'is_draft' => true
    ];
});
