<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Entities\Attendings\Attendings::class, function (Faker $faker) {
    return [
        "user_id" => function () {
            return \App\Entities\PlatformUsers\PlatformUsers::inRandomOrder()->first()->id;
        },
        "reservation_id" => function () {
            return \App\Entities\RoomReservations\RoomReservations::inRandomOrder()->first()->id;
        },
    ];
});
