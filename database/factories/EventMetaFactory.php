<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Entities\EventMeta\EventMeta::class, function (Faker $faker) {
    $start_date = $faker->date();
    return [
        'is_single' => true,
        'week_days' => [],
        'start_date' => $start_date,
        'end_date' => $start_date,
        'start_time' => $faker->time(),
        'end_time' => $faker->time(),
    ];
});
