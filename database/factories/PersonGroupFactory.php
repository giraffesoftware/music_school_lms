<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\PersonGroups\PersonGroups;
use Faker\Generator as Faker;

$factory->define(PersonGroups::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(rand(5,8))
    ];
});
