<?php

use Illuminate\Database\Seeder;

class EventMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entities\EventMeta\EventMeta::insert(
            [
                [
                  "event_meta" => json_encode(["data" => "test","some" => "data"])
                ],
                [
                    "event_meta" => json_encode(["data2" => "ssvsvs","some" => "data"])
                ],
                [
                    "event_meta" => json_encode(["data3" => "svsvs","some" => "data"])
                ],
                [
                    "event_meta" => json_encode(["data4" => "vvvvvv","some" => "data"])
                ],
                [
                    "event_meta" => json_encode(["data5" => "rwrwrwr","some" => "data"])
                ],
            ]
        );
    }
}
