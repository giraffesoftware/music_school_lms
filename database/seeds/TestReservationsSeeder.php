<?php

use Illuminate\Database\Seeder;

class TestReservationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventMeta = factory(\App\Entities\EventMeta\EventMeta::class)->create();

        \App\Entities\RoomReservations\RoomReservations::insert([
            [
                "event_name" => null,
                "room_id" => 2,
                "start_datetime" => \Carbon\Carbon::today()->setTime(14, 0),
                "end_datetime" => \Carbon\Carbon::today()->setTime(15, 0),
                "category_id" => \App\Entities\Categories\Categories::LESSON,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "subject_id" => 6,
                "event_meta_id" => $eventMeta->id,
            ],
            [
                "event_name" => "Test event 2",
                "room_id" => 2,
                "start_datetime" => \Carbon\Carbon::yesterday()->setTime(15, 0),
                "end_datetime" => \Carbon\Carbon::yesterday()->setTime(16, 0),
                "category_id" => \App\Entities\Categories\Categories::RESERVATION,
                "subject_id" => null,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "event_meta_id" => $eventMeta->id,
            ],
            [
                "event_name" => "Test event 3",
                "room_id" => 1,
                "start_datetime" => \Carbon\Carbon::yesterday()->setTime(14, 0),
                "end_datetime" => \Carbon\Carbon::yesterday()->setTime(15, 0),
                "category_id" => \App\Entities\Categories\Categories::CONCERT,
                "subject_id" => null,
                "event_meta_id" => $eventMeta->id,
                "status_id" => \App\Entities\Status\Status::BOOKED,
            ],
            [
                "event_name" => "Test event 4",
                "room_id" => 1,
                "start_datetime" => \Carbon\Carbon::tomorrow()->setTime(12, 0),
                "end_datetime" => \Carbon\Carbon::tomorrow()->setTime(14, 0),
                "category_id" => \App\Entities\Categories\Categories::MAINTENANCE,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "subject_id" => null,
                "event_meta_id" => $eventMeta->id,
            ],
            [
                "event_name" => null,
                "room_id" => 1,
                "start_datetime" => \Carbon\Carbon::now()->setTime(14, 0),
                "end_datetime" => \Carbon\Carbon::now()->setTime(15, 0),
                "category_id" => \App\Entities\Categories\Categories::LESSON,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "subject_id" => 15,
                "event_meta_id" => $eventMeta->id,
            ],
            [
                "event_name" => null,
                "room_id" => 6,
                "start_datetime" => \Carbon\Carbon::now()->setTime(11, 0),
                "end_datetime" => \Carbon\Carbon::now()->setTime(12, 0),
                "category_id" => \App\Entities\Categories\Categories::LESSON,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "subject_id" => 6,
                "event_meta_id" => $eventMeta->id,
            ],
            [
                "event_name" => null,
                "room_id" => 6,
                "start_datetime" => \Carbon\Carbon::tomorrow()->setTime(15, 0),
                "end_datetime" => \Carbon\Carbon::tomorrow()->setTime(16, 0),
                "category_id" => \App\Entities\Categories\Categories::LESSON,
                "status_id" => \App\Entities\Status\Status::BOOKED,
                "subject_id" => 14,
                "event_meta_id" => $eventMeta->id,
            ]
            ]
        );
    }
}
