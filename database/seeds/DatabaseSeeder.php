<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SetSubjectsSeeder::class);
        $this->call(SetCategoriesSeeder::class);
        $this->call(StatusesSeeder::class);
        $this->call(SetRoomsGroupsSeeder::class);
        $this->call(SetRoomsSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(PersonGroupsSeeder::class);
        $this->call(PlatformUsersSeeder::class);
    }
}
