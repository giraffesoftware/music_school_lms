<?php

use Illuminate\Database\Seeder;
use App\Entities\PersonGroupMembers\PersonGroupMembers;
use App\Entities\PlatformUsers\PlatformUsers;
use App\Entities\PersonGroups\PersonGroups;

class PersonGroupsSeeder extends Seeder
{
    use \Illuminate\Foundation\Testing\WithFaker;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonGroups::insert(
            [
                [
                    "name" => "Guitar"
                ],
                [
                    "name" => "Vocals"
                ],
                [
                    "name" => "Piano"
                ],
                [
                    "name" => "Drums"
                ],
            ]
        );
    }
}
