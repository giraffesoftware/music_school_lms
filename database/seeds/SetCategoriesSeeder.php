<?php

use Illuminate\Database\Seeder;
use App\Entities\Categories\Categories;

class SetCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('categories')->delete();

        Categories::insert([
                [
                    "id" => Categories::LESSON,
                    "name" => "Lesson",
                ],
                [
                    "id" => Categories::RESERVATION,
                    "name" => "Reservation",
                ],
                [
                    "id" => Categories::CONCERT,
                    "name" => "Concert",
                ],
                [
                    "id" => Categories::MAINTENANCE,
                    "name" => "Maintenance",
                ]
            ]
        );
    }
}
