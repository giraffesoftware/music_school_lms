<?php

use Illuminate\Database\Seeder;
use App\Entities\RoomGroups\RoomGroups;

class SetRoomsGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoomGroups::insert([
                ['id' => 1, "name" => "1.stāvs"],
                ['id' => 2, "name" => "2.stāvs"],
                ['id' => 3, "name" => "3.stāvs"]
            ]
        );
    }
}
