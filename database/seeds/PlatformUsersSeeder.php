<?php

use Illuminate\Database\Seeder;

class PlatformUsersSeeder extends Seeder
{
    private $currentUsersCount = 0;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Entities\PlatformUsers\PlatformUsers::class, 5)->create()
            ->each(function (\App\Entities\PlatformUsers\PlatformUsers $platformUsers){
                $platformUsers->attachRole(\App\Role::where('name', 'teacher')->first());
            });

        $totalCount = 5;
        $groups = App\Entities\PersonGroups\PersonGroups::all();
        $countPerGroup = $totalCount / $groups->count();


        $groups->each(function($group) use ($countPerGroup, $totalCount){
            // in last group we just take all left users
            $countPerThisGroup = ($totalCount - $this->currentUsersCount - intval($countPerGroup) >= $countPerGroup)?
                intval($countPerGroup) : $totalCount - $this->currentUsersCount;

            $this->currentUsersCount += $countPerThisGroup;

            factory(\App\Entities\PlatformUsers\PlatformUsers::class, $countPerThisGroup)->create([
                'person_group_id' => $group->id
            ])->each(function(\App\Entities\PlatformUsers\PlatformUsers $platformUser){
                $platformUser->attachRole(\App\Role::where('name', 'student')->first());
            });
        });

        //laratrust default student user
        $studentUser = \App\Entities\PlatformUsers\PlatformUsers::students()->first();

        if($studentUser){
            $studentUser->person_group_id = $groups->first()->id;
            $studentUser->save();
        }
        factory(\App\Entities\PlatformUsers\PlatformUsers::class, 5)->create()
            ->each(function (\App\Entities\PlatformUsers\PlatformUsers $platformUser){
                $platformUser->attachRole(\App\Role::where('name', 'teacher')->first());
            });
    }
}
