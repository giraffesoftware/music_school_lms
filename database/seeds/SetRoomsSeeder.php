<?php

use Illuminate\Database\Seeder;
use App\Entities\Rooms\Rooms;

class SetRoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roomGroupOneNumbers = ['116', '117', '119', '120', '121', 'Mazā zāle', 'Lielā zāle'];
        $roomGroupTwoNumbers = ['205', '206', '207', '208', '209', '210', '211', '212', '213', '214', '215', '216', '217',
            '218', '219', '220', '221', '222', '223', '224', '225', '226', '228', '229', '230', '231', '232'];
        $roomGroupThreeNumbers = ['304', '305', '306', '307', '308', '309', '310', '311', '312', '313', '314', '315',
            '316', '317', '318', '319', '320', '321', '322', '323', '331', '332', '333', '334', '337', '338',
            '339', '340', '342', '343'];

        $this->insertData($roomGroupOneNumbers,1);
        $this->insertData($roomGroupTwoNumbers,2);
        $this->insertData($roomGroupThreeNumbers,3);

    }

    private function insertData($dataToInsert,$roomGroupId)
    {
        foreach ($dataToInsert as $room)
        {
            Rooms::insert([
                'room_group_id' => $roomGroupId,
                'room_number' => $room,
            ]);
        }
    }
}
