<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlatformUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platform_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable()->unique();
            $table->string('password')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('middle_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('e_klass_id')->nullable()->unique();
            $table->string('e_klass_personal_code')->nullable();

            $table->unsignedInteger('person_group_id')->nullable();
            $table->foreign('person_group_id')->references('id')->on('person_groups')
                ->onDelete('set null');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_users');
    }
}
