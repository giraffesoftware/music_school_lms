<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_draft')->default(true);
            $table->string('event_name',150)->nullable();
            $table->integer('room_id')->unsigned();
            $table->datetime('start_datetime');
            $table->datetime('end_datetime');
            $table->integer('category_id')->unsigned();
            $table->integer('status_id')->default(\App\Entities\Status\Status::BOOKED)->unsigned();
            $table->integer('subject_id')->unsigned()->nullable();
            $table->integer('event_meta_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::table('room_reservations', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('event_meta_id')->references('id')->on('event_meta')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_reservations');
    }
}
