<?php

namespace Tests\Feature;

use App\Entities\Categories\Categories;
use App\Entities\EventMeta\EventMeta;
use App\Entities\Subjects\Subjects;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventMetaTest extends TestCase
{
    use WithFaker, DatabaseTransactions;

    /** @test * */
    public function createSingleEventMetaUnauthorized()
    {
        $data = $this->generateSingleEventMetaData();

        $response = $this->json('POST', route('event-meta.create'), $data);

        $response->assertStatus(401);
    }

    /** @test * */
    public function createSingleEventMetaWithLessonCategory()
    {
        $data = $this->generateSingleEventMetaData();
        $data['category_id'] = Categories::LESSON;

        $response = $this->actingAs($this->teachers->first())->json('POST', route('event-meta.create'),
            $data);
        $response->assertStatus(200);
        $this->singleEventMetaAssertings($response, $data);
    }

    /** @test * */
    public function createSingleEventMetaAsTeacher()
    {
        $data = $this->generateSingleEventMetaData();

        $response = $this->actingAs($this->teachers->first())->json('POST', route('event-meta.create'),
            $data);
        $response->assertStatus(200);
        $this->singleEventMetaAssertings($response, $data);
    }

    /** @test * */
    public function createSingleEventMetaWithReservationCategoryAsStudent()
    {
        $data = $this->generateSingleEventMetaData();
        $data['category_id'] = Categories::RESERVATION;

        $response = $this->actingAs($this->students->first())->json('POST', route('event-meta.create'),
            $data);
        $response->assertStatus(200);
        $this->singleEventMetaAssertings($response, $data);
    }

    /** @test * */
    public function createSingleEventMetaWithConcertCategoryAsStudent()
    {
        $data = $this->generateSingleEventMetaData();
        $data['category_id'] = Categories::CONCERT;

        $response = $this->actingAs($this->students->first())->json('POST', route('event-meta.create'),
            $data);
        $response->assertStatus(403);
    }

    /** @test * */
    public function createRecurringEventMetaAsTeacher()
    {
        $data = $this->generateReccurringEventMetaData();

        $response = $this->actingAs($this->students->first())->json('POST', route('event-meta.draft'), $data);

        $response->assertStatus(403);
    }

    /** @test * */
    public function createRecurringEventMeta()
    {
        $data = $this->generateReccurringEventMetaData();

        $response = $this->actingAs($this->admins->first())->json('POST', route('event-meta.draft'), $data);

        $response->assertStatus(200);

        foreach ($this->students as $student) {
            $response->assertSee($student->name);
        }

        foreach ($this->teachers as $teacher) {
            $response->assertSee($teacher->name);
        }

        $response->assertSee($this->rooms->first()->room_number);
        $response->assertSee($this->rooms->first()->description);

        $currentDate = $data['start_date']->copy();

        $firstWeekLessonDates = [];
        $firstWeekFinishDate = $currentDate->copy()->addWeek();
        while ($currentDate->lte($data['end_date']) && $currentDate->lt($firstWeekFinishDate)) {
            if (in_array($currentDate->dayOfWeek, $data['week_days'])) {
                $firstWeekLessonDates[] = $currentDate->copy();
            }

            $currentDate->addDay();
        }

        $startDates = [];
        $endDates = [];
        for ($i = 0; ; $i++) {
            foreach ($firstWeekLessonDates as $firstWeekLessonDate) {
                $currentDate = $firstWeekLessonDate->copy()->addWeeks($data['interval'] * $i);

                if ($currentDate->gt($data['end_date'])) {
                    break 2;
                }

                $startDate = $this->makeDatetimeFromDateAndTime($currentDate, $data['start_time']);
                $endDate = $this->makeDatetimeFromDateAndTime($currentDate, $data['end_time']);

                $startDates[] = $startDate;
                $endDates[] = $endDate;

                $response->assertSee($startDate->toDateTimeString());
                $response->assertSee($endDate->toDateTimeString());

                $this->assertDatabaseHas('room_reservations', [
                    'is_draft' => true,
                    'room_id' => $this->rooms->first()->id,
                    'start_datetime' => $startDate->toDateTimeString(),
                    'end_datetime' => $endDate->toDateTimeString(),
                    'category_id' => Categories::LESSON,
                    'subject_id' => $this->subjects->first()->id,
                ]);
            }
        }

        $response->assertSee(Categories::LESSON);

        $response->assertSee($this->statuses->first()->name);
        $response->assertSee($this->statuses->first()->color);

        $response->assertSee(json_encode($this->subjects->first()->name));


        //check submit
        $eventMeta = EventMeta::where('is_single', false)
            ->where('interval', $data['interval'])
            ->whereDate('start_date', $data['start_date']->toDateString())
            ->whereDate('end_date', $data['end_date']->toDateString())
            ->where('start_time', $data['start_time'])
            ->where('end_time', $data['end_time'])->first();

        $response = $this->actingAs($this->admins->first())->json('POST', route('event-meta.submit', $eventMeta->id));

        $response->assertSuccessful();

        foreach ($startDates as $key => $startDate) {
            $this->assertDatabaseHas('room_reservations', [
                'is_draft' => false,
                'room_id' => $this->rooms->first()->id,
                'start_datetime' => $startDate->toDateTimeString(),
                'end_datetime' => $endDates[$key]->toDateTimeString(),
                'category_id' => Categories::LESSON,
                'status_id' => $this->statuses->first()->id,
            ]);
        }
    }

    private function mockData()
    {
        $this->makeStudents(3);
        $this->makeTeachers(3);
        $this->makeAdmins();
        $this->makeCategories();
        $this->makeSubjects();
        $this->makeStatuses();
        $this->makeRooms();
    }

    private function getRandomTimeBetween(int $minHours, int $maxHours)
    {
        return date('H:i', random_int($minHours * 3600, $maxHours * 3600));
    }

    /**
     * @param Carbon $date
     * @param string $time
     * @return mixed
     */
    private function makeDatetimeFromDateAndTime(Carbon $date, string $time)
    {
        return $date->copy()->setTime(date('H', strtotime($time)), date('i', strtotime($time)));
    }

    private function makeRandomWeekDaysArray()
    {
        $number = -1;
        $res = [];
        do {
            $number = $this->faker->numberBetween($number + 1, 6);

            $res[] = $number;
        } while ($number < 6);

        return $res;
    }

    private function generateSingleEventMetaData()
    {
        $this->mockData();
        $startTime = $this->getRandomTimeBetween(7, 18);
        $startHour = intval(date('H', strtotime($startTime)));

        return [
            'event_name' => $this->faker->sentence(5),
            'students' => $this->students->pluck('id'),
            'teachers' => $this->teachers->pluck('id'),
            'room_id' => $this->rooms->first()->id,
            'start_date' => Carbon::tomorrow(),
            'start_time' => $startTime,
            'end_time' => $this->getRandomTimeBetween($startHour + 1, 20),
            'category_id' => $this->categories->random()->id,
            'status_id' => $this->statuses->first()->id,
            'subject_id' => $this->subjects->first()->id,
            'description' => $this->faker->text(),
        ];
    }

    private function generateReccurringEventMetaData()
    {
        $this->mockData();
        $startTime = $this->getRandomTimeBetween(7, 18);
        $startHour = intval(date('H', strtotime($startTime)));

        return [
            'students' => $this->students->pluck('id'),
            'teachers' => $this->teachers->pluck('id'),
            'room_id' => $this->rooms->first()->id,
            'week_days' => $this->makeRandomWeekDaysArray(),
            'interval' => $this->faker->numberBetween(1, 3),
            'start_date' => today()->startOfMonth(),
            'end_date' => today()->endOfMonth(),
            'start_time' => $startTime,
            'end_time' => $this->getRandomTimeBetween($startHour + 1, 20),
            'category_id' => Categories::LESSON,
            'status_id' => $this->statuses->first()->id,
            'subject_id' => $this->subjects->first()->id,
        ];
    }

    private function singleEventMetaAssertings($response, $data): void
    {
        $startDate = $this->makeDatetimeFromDateAndTime($data['start_date'], $data['start_time']);
        $endDate = $this->makeDatetimeFromDateAndTime($data['start_date'], $data['end_time']);

        $databaseAssertings = [
            'is_draft' => false,
            'room_id' => $this->rooms->first()->id,
            'start_datetime' => $startDate->toDateTimeString(),
            'end_datetime' => $endDate->toDateTimeString(),
            'category_id' => $data['category_id'],
            'status_id' => $this->statuses->first()->id,
        ];

        if ($data['category_id'] === Categories::LESSON) {
            $response->assertSee(json_encode(Subjects::find($data['subject_id'])->name));
            $databaseAssertings['subject_id'] = $data['subject_id'];
        } else {
            $response->assertSee($data['event_name']);
            $response->assertSee($data['description']);
            $databaseAssertings['event_name'] = $data['event_name'];
            $databaseAssertings['description'] = $data['description'];
        }

        foreach ($this->students as $student) {
            $response->assertSee($student->name);
        }

        foreach ($this->teachers as $teacher) {
            $response->assertSee($teacher->name);
        }

        $response->assertSee($this->rooms->first()->room_number);
        $response->assertSee($this->rooms->first()->description);


        $response->assertSee($startDate->toDateTimeString());
        $response->assertSee($endDate->toDateTimeString());

        $response->assertSee(Categories::find($data['category_id'])->name);

        $response->assertSee($this->statuses->first()->name);
        $response->assertSee($this->statuses->first()->color);

//        $response->assertSee($this->subjects->first()->name);
        $this->assertDatabaseHas('room_reservations', $databaseAssertings);
    }
}
