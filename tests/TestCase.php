<?php

namespace Tests;

use App\Entities\Categories\Categories;
use App\Entities\PlatformUsers\PlatformUsers;
use App\Entities\Rooms\Rooms;
use App\Entities\Status\Status;
use App\Entities\Subjects\Subjects;
use App\Services\EKLassService;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Collection;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @var Collection $students
     * @var Collection $teachers
     * @var Collection $rooms
     * @var Collection $categories
     * @var Collection $subjects
     */
    public $students, $teachers, $rooms, $categories, $statuses, $subjects, $admins;

    public function makeStudents(int $count = 1)
    {
        $this->students = factory(PlatformUsers::class, $count)->create();
        $this->students->each(function ($student) {
            $student->attachRole('student');
        });
    }

    public function makeTeachers(int $count = 1)
    {
        $this->teachers = factory(PlatformUsers::class, $count)->create();
        $this->teachers->each(function ($teacher) {
            $teacher->attachRole('teacher');
        });
    }

    public function makeAdmins(int $count = 1)
    {
        $this->admins = factory(PlatformUsers::class, $count)->create();
        $this->admins->each(function ($teacher) {
            $teacher->attachRole('administrator');
        });
    }

    public function makeRooms(int $count = 1)
    {
        $this->rooms = factory(Rooms::class, $count)->create();
    }

    public function makeCategories()
    {
        $this->categories = Categories::inRandomOrder()->get();
    }

    public function makeStatuses()
    {
        $this->statuses = Status::inRandomOrder()->get();
    }

    public function makeSubjects()
    {
        $this->subjects = Subjects::inRandomOrder()->get();
    }

}
