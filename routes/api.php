<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('permissions', 'PermissionsController@get')->name('get-permissions');
Route::get('roles', 'PermissionsController@getRoles')->name('get-roles');

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('login-using-e-class', 'AuthController@loginUsingEKlass')->name('login-using-e-klass');
    Route::post('logout', 'AuthController@logout')->name('logout')->middleware('auth:api');
    Route::get('me', 'AuthController@me')->name('me')->middleware('auth:api');
    Route::post('forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.forgot');
    Route::post('reset-password', 'Auth\ResetPasswordController@reset')->name('password.reset');

});

Route::group([
    'middleware' => 'auth:api',

], function () {
    Route::group(['prefix' => 'reservations'], function () {
        Route::group(['prefix' => 'reservation'], function () {
            Route::post('/create', 'ReservationController@createReservation')->middleware('permission:create-event');
            Route::get('/{roomReservation}', 'ReservationController@getReservation')
                ->middleware('permission:read-event');
            Route::delete('/multiply', 'ReservationController@deleteMultiplyReservations')
                ->middleware('permission:delete-event');
            Route::delete('/{roomReservation}', 'ReservationController@deleteReservation')
                ->middleware('permission:delete-event');
            Route::put('/{roomReservation}', 'ReservationController@editReservation')
                ->middleware('permission:update-event');
        });
        Route::get('/all', 'ReservationController@getAllReservations')
            ->middleware('permission:read-event');
        Route::get('/by_rooms', 'ReservationController@getRoomsReservations')
            ->middleware('permission:read-event');
        Route::get('/by_date', 'ReservationController@getReservationsByDate')
            ->middleware('permission:read-event');
        Route::get('/by_user/{user}/by_date', 'ReservationController@getReservationsByUserAndDate')
            ->middleware('has-individual-plan-permissions');
    });

    Route::group(['prefix' => 'event-meta', 'as' => 'event-meta.'], function () {
        //permissions in request class
        Route::post('/create', 'EventMetaController@createEventMeta')->name('create');
        Route::post('/draft', 'EventMetaController@draftEventMeta')->name('draft')
            ->middleware('permission:create-event_meta');
        Route::post('/submit/{eventMeta}', 'EventMetaController@submitEventMeta')->name('submit')
            ->middleware('permission:create-event_meta');
        Route::get('/{eventMeta}', 'EventMetaController@getEventMeta')->name('get')
            ->middleware('permission:read-event');
        Route::delete('/{eventMeta}', 'EventMetaController@deleteEventMeta')->name('delete')
            ->middleware('permission:delete-event');
        Route::get('/all', 'EventMetaController@getAllEventMeta')->name('all')
            ->middleware('permission:read-event');
    });

    Route::group(['prefix' => 'rooms'], function () {
        Route::group(['prefix' => 'room'], function ($router) {
            //Route::get('/{id}', 'ReservationController@getReservation');
        });
        Route::get('/', 'RoomGroupsController@getRoomGroupsWithRooms')
            ->middleware('permission:read-group|read-room_group');
        Route::get('/get_schedule', 'RoomController@getRoomScheduleByPeriod')
            ->middleware('permission:read-schedule');
    });

    Route::get('/categories', 'CategoriesController@getCategories')
        ->middleware('permission:read-category');
    Route::get('/statuses', 'StatusesController@index')
        ->middleware('permission:read-status');
    Route::get('/subjects', 'SubjectsController@getSubjects')
        ->middleware('permission:read-subject');
    Route::get('/students', 'PlatformUsersController@getStudents')
        ->middleware('permission:read-user');
    Route::get('/teachers', 'PlatformUsersController@getTeachers')
        ->middleware('permission:read-user');

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {

        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {

            Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

                Route::post('/create', 'Admin\AdminUserController@createUser')->name('create')
                    ->middleware('permission:create-user');
                Route::delete('/multiply', 'Admin\AdminUserController@deleteMultiplyUsers')->name('deleteMultiply')
                    ->middleware('permission:delete-user');
                Route::delete('/{user}', 'Admin\AdminUserController@deleteUser')->name('delete')
                    ->middleware('permission:delete-user');
                Route::patch('/{user}', 'Admin\AdminUserController@editUser')->name('update')
                    ->middleware('permission:update-user');

            });

            Route::get('/', 'Admin\AdminUserController@getUsers')->name('all')
                ->middleware('permission:read-user');
            Route::get('/archive', 'Admin\AdminUserController@getArchiveUsers')->name('archive')
                ->middleware('permission:read-user');
            Route::get('/{user}', 'Admin\AdminUserController@getUser')->name('one')
                ->middleware('permission:read-user');

        });

        Route::group(['prefix' => 'groups', 'as' => 'groups.'], function () {

            Route::group(['prefix' => 'group', 'as' => 'group.'], function () {

                Route::post('/create', 'Admin\AdminGroupController@createGroup')->name('create')
                    ->middleware('permission:create-user_group');
                Route::delete('/multiply', 'Admin\AdminGroupController@deleteMultiplyGroups')->name('deleteMultiply')
                    ->middleware('permission:delete-user_group');
                Route::delete('/{group}', 'Admin\AdminGroupController@deleteGroup')->name('delete')
                    ->middleware('permission:delete-user_group');
                Route::put('/{group}', 'Admin\AdminGroupController@editGroup')->name('update')
                    ->middleware('permission:update-user_group');
                Route::post('/{group}/assign', 'Admin\AdminGroupController@assignStudents')->name('assign')
                    ->middleware('permission:update-user_group');
                Route::post('/{group}/detach', 'Admin\AdminGroupController@detachStudents')->name('detach')
                    ->middleware('permission:update-user_group');

            });

            Route::get('/', 'Admin\AdminGroupController@getGroups')->name('all')
                ->middleware('permission:read-user_group');
            Route::get('/students', 'Admin\AdminGroupController@getReadyToAttachStudents')->name('students')
                ->middleware('permission:read-user');
            Route::get('/{group}', 'Admin\AdminGroupController@getGroup')
                ->middleware('permission:read-user_group');

        });

        Route::group(['prefix' => 'subjects', 'as' => 'subjects.'], function () {

            Route::group(['prefix' => 'subject', 'as' => 'subject.'], function () {

                Route::post('/create', 'Admin\AdminSubjectController@createSubject')->name('create')
                    ->middleware('permission:create-subject');
                Route::delete('/multiply', 'Admin\AdminSubjectController@deleteMultiplySubjects')->name('deleteMultiply')
                    ->middleware('permission:delete-subject');
                Route::delete('/{subject}', 'Admin\AdminSubjectController@deleteSubject')->name('delete')
                    ->middleware('permission:delete-subject');
                Route::put('/{subject}', 'Admin\AdminSubjectController@editSubject')->name('update')
                    ->middleware('permission:update-subject');

            });

            Route::get('/', 'Admin\AdminSubjectController@getSubjects')->name('all')
                ->middleware('permission:read-subject');
            Route::get('/{subject}', 'Admin\AdminSubjectController@getSubject')->name('one')
                ->middleware('permission:read-subject');

        });

    });
});
