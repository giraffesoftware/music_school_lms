const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}
module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias.set('@', resolve('src'));
    config.resolve.alias.set('@api', resolve('api'));
    config.resolve.alias.set('@views', resolve('src/views'));
    config.resolve.alias.set('@forms', resolve('src/components/Forms'));
    config.resolve.alias.set('@admin', resolve('src/views/Admin'));
    config.resolve.alias.set('@store', resolve('src/store'));
    config.resolve.alias.set('@router', resolve('src/router'));
    config.resolve.alias.set('@components', resolve('src/components'));
    config.resolve.alias.set('@utils', resolve('src/utils'));
    config.resolve.alias.set('@helpers', resolve('src/helpers'));
    config.resolve.alias.set('@vendor', resolve('node_modules'));
    config.resolve.alias.set('@config', resolve('src/config'));
    config.resolve.alias.set('@constants', resolve('src/constants'));
    config.resolve.alias.set('@mixins', resolve('src/mixins'));
    config.resolve.alias.set('@modals', resolve('src/components/Modals'));
    config.resolve.alias.set('@buttons', resolve('src/components/Shared/Buttons'));
    config.resolve.alias.set('@icons', resolve('src/components/Shared/Icons'));
  },
  transpileDependencies: [
    'vuetify',
  ],
  css: {
    loaderOptions: {
      scss: {
        prependData:
            '@import "@/styles/variables.scss";',
      },
    },
  },

  // proxy API requests to Valet during development
  devServer: {
    proxy: process.env.VUE_APP_URL,
  },

  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../public',

  // modify the location of the generated HTML file.
  // make sure to do this only in production.
  indexPath: process.env.NODE_ENV === 'production'
    ? '../resources/views/index.blade.php'
    : 'index.html',
};
