import http from '@utils/http';
import store from '@store';

export default {
  getRoles() {
    return new Promise((resolve, reject) => {
      http.get('/api/roles').then(
        ({ data }) => {
          store.dispatch('roles/setRoles', data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
};
