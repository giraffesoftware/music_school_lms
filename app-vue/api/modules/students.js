import http from '@utils/http';
import store from '@store';

export default {
  fetchData() {
    return new Promise((resolve, reject) => {
      http.get('/api/students').then(
        ({ data }) => {
          store.dispatch('students/setStudents', data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
};
