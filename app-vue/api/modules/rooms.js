import http from '@utils/http';
import store from '@store';

export default {
  fetchData() {
    return new Promise((resolve, reject) => {
      http.get('/api/rooms').then(
        ({ data }) => {
          store.dispatch('rooms/setRooms', data);
        },
      ).catch((error) => {
        reject(error);
      });
    });
  },
};
