const getters = {
  getSubjects: state => state.list,
  isLoaded: state => state.isLoaded,
};

const actions = {
  setSubjects({ commit }, data) {
    commit('SET_SUBJECTS', data);
  },
};

const mutations = {
  SET_SUBJECTS(state, data) {
    state.list = [...data];
  },
};

const state = {
  isLoaded: false,
  list: [],
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
