const getters = {
  getStatuses: state => state.list,
  isLoaded: state => state.isLoaded,
};

const actions = {
  setStatuses({ commit }, data) {
    commit('SET_STATUSES', data);
  },
};

const mutations = {
  SET_STATUSES(state, data) {
    state.list = [...data];
  },
};

const state = {
  isLoaded: false,
  list: [],
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
