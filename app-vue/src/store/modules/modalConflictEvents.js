const getters = {
  getVisible(state) {
    return state.show;
  },
  getEvents(state) {
    return state.events;
  },
  getIsShouldBeCleared(state) {
    return state.isShouldBeCleared;
  },
};

const actions = {
  setShow({ commit }, date) {
    commit('setShow', date);
  },
  setEvents({ commit }, date) {
    commit('setEvents', date);
  },
  setIsShouldBeCleared({ commit }, value) {
    commit('setIsShouldBeCleared', value);
  },
  clearEvents({ commit }) {
    commit('clearEvents');
  },
};

const mutations = {
  setShow(state, data) {
    state.show = data;
  },
  setEvents(state, { errors }) {
    state.events = {
      room: errors.room,
      teachers: errors.teachers,
      students: errors.students,
      end_datetime: errors.end_datetime,
    };
  },
  setIsShouldBeCleared(state, value) {
    state.isShouldBeCleared = value;
  },
  clearEvents(state) {
    state.events = {
      room: '',
      teachers: {},
      students: {},
      end_datetime: [],
    };
  },
};

const state = {
  show: false,
  isShouldBeCleared: false,
  events: {
    room: '',
    teachers: {},
    students: {},
    end_datetime: [],
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
