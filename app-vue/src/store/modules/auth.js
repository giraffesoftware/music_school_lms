import api from '@api';

const getters = {
  getIsAuthenticated(state) {
    return state.isAuthenticated;
  },
  getUser(state) {
    return state.user;
  },
  getPermissions(state) {
    return state.permissions;
  },
  getToken(state) {
    return state.userToken;
  },
};

const actions = {
  auth({ commit }) {
    commit('AUTH_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.auth().then(({ data }) => {
        resolve(data);
        commit('AUTH_SUCCESS', data);
      }).catch((e) => {
        console.log(e);
        commit('AUTH_FAILURE');
        reject(e);
      });
    });
  },
  authEclass({ commit }, { code }) {
    commit('LOGIN_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.authEclass(code).then(({ data }) => {
        resolve(data);
        console.log(data);
        commit('LOGIN_SUCCESS', data.access_token);
      }).catch((e) => {
        console.log(e);
        commit('LOGIN_FAILURE');
        reject(e);
      });
    });
  },
  permissions({ commit }) {
    commit('PERMISSIONS_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.permissions().then((response) => {
        resolve(response);
        commit('PERMISSIONS_SUCCESS', response);
      }).catch((e) => {
        console.log(e);
        commit('PERMISSIONS_FAILURE');
        reject(e);
      });
    });
  },
  login({ commit }, { email, password }) {
    commit('LOGIN_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.login({ email, password }).then((response) => {
        resolve(response);
        commit('LOGIN_SUCCESS', response.access_token);
      }).catch((e) => {
        console.log(e);
        commit('LOGIN_FAILURE');
        reject(e);
      });
    });
  },
  logout({ commit }) {
    commit('LOGOUT_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.logout().then(({ data }) => {
        localStorage.removeItem('user-token');
        resolve(data);
        commit('LOGOUT_SUCCESS');
      }).catch((e) => {
        console.log(e);
        commit('LOGOUT_FAILURE');
        reject(e);
      });
    });
  },
  register({ commit }, payload) {
    commit('REGISTER_REQUEST');
    return new Promise((resolve, reject) => {
      api.auth.register(payload).then((response) => {
        resolve(response);
        commit('REGISTER_SUCCESS', response.access_token);
      }).catch((e) => {
        console.log(e);
        commit('REGISTER_FAILURE');
        reject(e);
      });
    });
  },
};

const mutations = {
  AUTH_REQUEST() {},
  AUTH_SUCCESS(state, payload) {
    localStorage.setItem('isAuthenticated', true);
    state.user = { ...payload };
  },
  AUTH_FAILURE(state) {
    state.isAuthenticated = false;
    localStorage.removeItem('isAuthenticated');
  },
  LOGIN_REQUEST() {},
  LOGIN_SUCCESS(state, token) {
    state.userToken = token;
    localStorage.setItem('user-token', token);
  },
  LOGIN_FAILURE(state) {
    state.isAuthenticated = false;
  },
  LOGOUT_REQUEST() {},
  LOGOUT_SUCCESS() {
    localStorage.removeItem('isAuthenticated');
    localStorage.removeItem('user-token');
  },
  LOGOUT_FAILURE() {
    localStorage.removeItem('isAuthenticated');
    localStorage.removeItem('user-token');
  },
  REGISTER_REQUEST() {},
  REGISTER_SUCCESS(state, token) {
    state.userToken = token;
    localStorage.setItem('user-token', token);
  },
  REGISTER_FAILURE() {
    localStorage.removeItem('isAuthenticated');
  },
  PERMISSIONS_REQUEST() {},
  PERMISSIONS_SUCCESS(state, data) {
    state.permissions = data;
  },
  PERMISSIONS_FAILURE() {
    // localStorage.removeItem('isAuthenticated');
  },
};

const state = {
  userToken: localStorage.getItem('user-token') || null,
  isAuthenticated: localStorage.getItem('isAuthenticated') || null,
  user: {},
  permissions: {},
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
