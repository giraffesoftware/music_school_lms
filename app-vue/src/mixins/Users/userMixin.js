import { debounce } from 'lodash';

const configUser = {
  search: null,
  role_id: null,
  totalDesserts: 0,
  totalPages: 0,
};

export default {
  data: () => ({}),
  computed: {},
  methods: {
    _addEventListener() {
      this.$events.$on('fetchUsers', () => {
        this._updateTable(this.user);
      });
    },
    _removeEventListener() {
      this.$events.$off('fetchUsers');
    },
    _goToCreatePage() {
      this.$router.push('/admin/users/create');
    },
    _goToEditPage(id) {
      this.$router.push(`/admin/users/${id}/edit`);
    },
    async _fetchAllUsers() {
      this.$events.$emit('fetchUsers');
    },
    _resetUserParams() {
      this.user = { ...configUser };
    },
    async _removeUser(idUser) {
      const confirm = await this.$root.$confirm.open(
        this.$t('message.admin.delete'),
        this.$t('message.admin.are_you_sure_you_want_to_delete_a_user'),
      );
      if (confirm) {
        await this.$api.users.removeUser(idUser).catch(async (err) => {
          if (err.response.data.errors.event === 'This student or teacher is participant of active event') {
            const confirmActiveUser = await this.$root.$confirm.open(
              this.$t('message.admin.delete'),
              `${this.$t('message.admin.this_student_is_participant_of_active_event')}.
              ${this.$t('message.admin.are_you_sure_you_want_to_delete_a_user')}`,
            );

            if (confirmActiveUser) {
              await this.$api.users.removeUser(idUser, { force: true });
            }
          }
        });
        await this._fetchAllUsers();
      }
    },
    async _removeMultiplyUser(idsArray) {
      if (!idsArray.length) {
        this._showErrorNotify(this.$t('message.admin.please_select_users'));
        return;
      }

      await this.$api.users.removeMultiplyUser({ ids: idsArray }).catch(async (err) => {
        if (err.response.data.errors.event.message === 'Student(s) or Teacher(s) are(is) participant of active events') {
          const confirmActiveUser = await this.$root.$confirm.open(
            this.$t('message.admin.delete'),
            `${this.$t('message.admin.this_student_is_participant_of_active_event')}.
              ${this.$t('message.admin.are_you_sure_you_want_to_delete_a_user')}`,
          );

          if (confirmActiveUser) {
            await this.$api.users.removeMultiplyUser({ ids: idsArray, force: true });
          }
        }
      });
      await this._fetchAllUsers();
    },
    _updateTable(data) {
      return this.getUsers({
        page: data.page || 1,
        search: data.search || null,
        role_id: data.role_id || null,
      });
    },
    _prepareDataForUpdateTable(currentPage = 1) {
      return {
        page: currentPage,
        search: this.user.search,
        role_id: this.user.role_id,
      };
    },
    _changeSelectUsersRoles(roleId) {
      this.user.role_id = roleId;
      this._updateTable(this._prepareDataForUpdateTable());
    },
    _updatePage(currentPage) {
      this.user.page = currentPage;
      this._updateTable(this._prepareDataForUpdateTable(currentPage));
    },
    _findUsersByText: debounce(function (query) {
      this.user.search = query;
      this._updateTable(this._prepareDataForUpdateTable());
    }, 500),
  },
};
