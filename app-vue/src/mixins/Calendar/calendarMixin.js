export default {
  data: () => ({
    configViewCalendar: null,
  }),
  computed: {
    _getTimeOfDay() {
      const timesOfDay = [];

      for (
        let time = this.configViewCalendar.startTime;
        time <= this.configViewCalendar.endTime;
        time += this.configViewCalendar.interval) {
        timesOfDay.push(this._createTime(time));
      }

      return timesOfDay;
    },
  },
  methods: {
    _setConfigViewCalendar(settings) {
      this.configViewCalendar = Object.assign(settings, this.settings || {});
    },
    _createTime(data) {
      const times = data.toFixed(1).split('.');
      const calcMinutes = times[1] / 10 * 60;

      return {
        time: {
          hour: +times[0],
          minute: calcMinutes,
        },
      };
    },
    _getPositionEventOnHours(minutes, cellSize) {
      const cellMinutes = 60 * this.configViewCalendar.interval;
      return minutes * this.configViewCalendar[cellSize] / cellMinutes;
    },
    _changeDate({ date, time, day }) {
      const getHour = (time && time.hour) || 0;
      const getMinute = (time && time.minute) || 0;
      const getDay = day || 0;

      return this.$moment(date).set({
        hour: getHour,
        minute: getMinute,
        second: 0,
        millisecond: 0,
      }).add(getDay, 'days');
    },
    _onMouseOverByCalendarEvent(index, $event) {
      this.selectInfoEvent.index = index;

      setTimeout(() => {
        document.querySelector('.calendar').classList.add('static-header');
        const html = document.getElementsByTagName('html')[0];
        const diffHtmlScroll = html.scrollHeight - html.scrollTop - html.clientHeight;
        const calendarEventWidth = this.$refs.calendarEvent[0].$el.clientWidth;
        const calendarEventHeight = this.$refs.calendarEvent[0].$el.clientHeight;
        const { innerWidth, innerHeight } = window;

        const heightWithScoll = innerHeight + diffHtmlScroll;

        const eventElem = document.elementFromPoint($event.clientX, $event.clientY)
          .getBoundingClientRect();
        const isEventHorizontalFit = (eventElem.x + eventElem.width + calendarEventWidth + 30)
            > innerWidth;
        const isEventVerticalFit = (eventElem.y + calendarEventHeight) > heightWithScoll;
        const eventPosition = {
          left: isEventHorizontalFit ? `${eventElem.x - calendarEventWidth}px` : `${eventElem.x + eventElem.width}px`,
          top: isEventVerticalFit
            ? `${$event.clientY - calendarEventHeight}px`
            : `${$event.clientY - calendarEventHeight / 2}px`,
        };
        this.selectInfoEvent.direction = isEventHorizontalFit ? 'right' : 'left';
        this.selectInfoEvent.position = eventPosition;
      }, 10);
    },
    _onMouseLeaveByCalendarEvent() {
      document.querySelector('.calendar').classList.remove('static-header');
      this.selectInfoEvent.index = null;
      this.selectInfoEvent.position = null;
      this.selectInfoEvent.direction = null;
    },
  },
};
