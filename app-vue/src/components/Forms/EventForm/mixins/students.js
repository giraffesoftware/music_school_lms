export default {
  data: () => ({
    showStudentsError: false,
  }),
  computed: {
    studentsIsSelected() {
      return this.form.students.length !== 0;
    },
  },
  methods: {
    selectStudents(students) {
      this.showStudentsError = false;
      this.form.students = [...students];
    },
  },
};
