export default {
  data: () => ({
    showTeachersError: false,
  }),
  computed: {
    getTeachers: {
      get() {
        return this.$store.getters['teachers/getTeachers'];
      },
    },
    teachersIsSelected() {
      return this.form.teachers.length !== 0;
    },
    filtredTeachers() {
      return this.getTeachers.filter((teacher) => {
        let isValid = true;
        this.form.teachers.forEach((item) => {
          if (item.id === teacher.id) {
            isValid = false;
          }
        });
        return isValid;
      });
    },
  },
  methods: {
    teachersChanged(elem) {
      this.form.teachers.push(elem);
      this.showTeachersError = false;
    },
    teacherRemove(id) {
      this.form.teachers = this.form.teachers.filter(item => item.id !== id);
    },
  },
};
