export default {
  convertArrayObjectToArrayParam(arr, param) {
    return arr.map(item => item[param]);
  },
};
