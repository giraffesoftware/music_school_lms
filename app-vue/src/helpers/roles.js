import store from '@store';

const configRoles = {
  admin: 'administrator',
};

export default {
  checkRoles(role) {
    const user = store.getters['auth/getUser'];
    if (!user.roles) return false;

    const userRoles = user.roles;

    return userRoles.some(item => item.name === role);
  },
  isAdmin() {
    return this.checkRoles(configRoles.admin);
  },
};
